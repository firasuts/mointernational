<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* FOOTER TEMPLATE
* ========================
*     
**/




if ( get_post_meta( get_the_ID(), 'cocoon_show_footer', 'true' ) != 'hide' ) {

    $footer_skin        = cocoon_get_option( 'cocoon_footer_skin' );
    $footer_info        = cocoon_get_option( 'cocoon_footer_info' );
    $footer_sidebar_1   = cocoon_get_option( 'cocoon_footer_sidebar_1' );
    $footer_sidebar_2   = cocoon_get_option( 'cocoon_footer_sidebar_2' );
    $footer_sidebar_3   = cocoon_get_option( 'cocoon_footer_sidebar_3' );
    $footer_sidebar_4   = cocoon_get_option( 'cocoon_footer_sidebar_4' );
    ?>



    <!-- =============== START OF FOOTER =============== -->
    <footer class="footer1 <?php echo esc_attr($footer_skin); ?>">

        <?php if ( $footer_info == true ) {
            if ( get_post_meta( get_the_ID(), 'cocoon_show_footer_widgets', 'true' ) != 'hide' ) {
                if ( is_active_sidebar('footer-widget-area') || is_active_sidebar('footer-widget-area-2') || is_active_sidebar('footer-widget-area-3') || is_active_sidebar('footer-widget-area-4') ) { ?>

                    <!-- ===== START OF FOOTER WIDGET AREA ===== -->
                    <div class="footer-widget-area">
                        <div class="container">
                            <div class="row">


                                <?php if( $footer_sidebar_1 != 'disabled' ) : ?>
                                    <!-- Start of Footer Widget Area 1 -->
                                    <div class="<?php echo esc_attr( $footer_sidebar_1 ) ?>">
                                        <?php dynamic_sidebar( 'footer-widget-area' ); ?>
                                    </div>
                                    <!-- End of Footer Widget Area 1 -->
                                <?php endif; ?>


                                <?php if( $footer_sidebar_2 != 'disabled' ) : ?>
                                    <!-- Start of Footer Widget Area 2 -->
                                    <div class="<?php echo esc_attr( $footer_sidebar_2 ) ?>">
                                        <?php dynamic_sidebar( 'footer-widget-area-2' ); ?>
                                    </div>
                                    <!-- End of Footer Widget Area 2 -->
                                <?php endif; ?>


                                <?php if( $footer_sidebar_3 != 'disabled' ) : ?>
                                    <!-- Start of Footer Widget Area 3 -->
                                    <div class="<?php echo esc_attr( $footer_sidebar_3 ) ?>">
                                        <?php dynamic_sidebar( 'footer-widget-area-3' ); ?>
                                    </div>
                                    <!-- End of Footer Widget Area 3 -->
                                <?php endif; ?>


                                <?php if( $footer_sidebar_4 != 'disabled' ) : ?>
                                    <!-- Start of Footer Widget Area 4 -->
                                    <div class="<?php echo esc_attr( $footer_sidebar_4 ) ?>">
                                        <?php dynamic_sidebar( 'footer-widget-area-4' ); ?>
                                    </div>
                                    <!-- End of Footer Widget Area 4 -->
                                <?php endif; ?>


                            </div>
                        </div>
                    </div>
                    <!-- ===== END OF FOOTER WIDGET AREA ===== -->

                <?php } // End of Widgets active check
            } // End if cocoon_show_footer_widgets    
        } ?>


        <!-- ===== START OF FOOTER COPYRIGHT AREA ===== -->
        <div class="footer-copyright-area ptb30">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="d-flex">

                            <div class="copyright mr-auto">
                                <?php 
                                $copyright = cocoon_get_option( 'cocoon_copyrights' ); 
                                echo wp_kses_post($copyright);
                                ?>
                            </div>
                            
                            <?php
                            $footericons = cocoon_get_option( 'cocoon_footer_socials', array() );
                            if ( !empty( $footericons ) ) {
                                echo '<ul class="social-btns">';
                                foreach( $footericons as $icon ) {
                                    echo '<li>
                                            <a class="social-btn-roll ' . esc_attr($icon['social_type']) . '" href="' . esc_url($icon['link_url']) . '" target="_blank">
                                                <div class="social-btn-roll-icons">
                                                    <i class="social-btn-roll-icon fa fa-' . esc_attr($icon['social_type']) . '"></i>
                                                    <i class="social-btn-roll-icon fa fa-' . esc_attr($icon['social_type']) . '"></i>
                                                </div>
                                            </a>
                                            </li>';
                                }
                                echo '</ul>';
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ===== END OF FOOTER COPYRIGHT AREA ===== -->

    </footer>
    <!-- =============== END OF FOOTER =============== -->

<?php } // End if cocoon_show_footer ?>


</div>
<!-- End of Website wrapper -->


<?php wp_footer(); ?>

</body>
</html>
<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* HEADER TEMPLATE
* ========================
*     
**/
?>



<!DOCTYPE html>
<html <?php get_language_attributes(); ?> >

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <!-- Mobile viewport optimized -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>
</head>  
    

<body <?php body_class(); ?>>
    
    <!-- Start Website wrapper -->
    <?php $body_layout = cocoon_get_option('cocoon_body_style'); ?>
    <div class="wrapper <?php echo esc_attr($body_layout); ?>">
    
    
    
        <?php
        if ( get_post_meta( get_the_ID(), 'cocoon_show_header', 'true' ) != 'hide' ) { ?>

            <!-- =============== Start of Header Navigation =============== -->
            <?php 
                $header = cocoon_get_option( 'cocoon_header_style' );
                get_template_part( 'templates/header/' . $header );
            ?>
            <!-- =============== End of Header Navigation =============== -->
            
            <?php get_template_part( '/templates/extra/popups'); ?>
        
        <?php
        } // End if cocoon_show_header
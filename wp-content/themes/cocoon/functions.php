<?php
/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* MAIN FUNCTION FILE
* ========================
*     
**/





require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/theme-support.php';
require get_template_directory() . '/inc/enqueue.php';
require get_template_directory() . '/inc/breadcrumb.php';
require get_template_directory() . '/inc/dynamic-css.php';
require get_template_directory() . '/inc/widgets.php';
require get_template_directory() . '/inc/plugins/activate-plugins.php';
require get_template_directory() . '/inc/mega-menu/class-mega-menu.php';
require get_template_directory() . '/inc/mega-menu/class-mega-menu-walker.php';
require get_template_directory() . '/inc/importer/start-page/admin-menu.php';
require get_template_directory() . '/inc/importer/start-page/redirect.php';
require get_template_directory() . '/inc/importer/admin-tpl/extra.php';

if ( cocoon_wc_is_activated() ) {
    require get_template_directory() . '/inc/woocommerce.php';
}
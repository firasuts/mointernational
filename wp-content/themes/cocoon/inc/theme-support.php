<?php

/**
*
* @package Cocoon
*
* @since 1.0
*
* ========================
* THEME MAIN FUNCTIONS
* ========================
*
**/




/*
=====================================================
COCOON THEME SETUP
=====================================================
*/

function cocoon_setup() {

    /* Set the content width */
    $GLOBALS[ 'content_width' ] = apply_filters( 'cocoon_content_width', 680 );
    if ( ! isset( $content_width ) ) {
        $content_width = 680;
    }


    /* Make theme available for translation */
	load_theme_textdomain( 'cocoon', get_template_directory() . '/lang' );

    /* Enable Support for Post Thumbnails */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size(840, 350, true); //size of thumbnails
    add_image_size( 'cocoon-blog', 1000, 563, true );

    /* Enable Support for Post Formats */
    add_theme_support('post-formats', array('audio', 'image', 'gallery', 'quote', 'video'));

    /* Change default markup to output valid HTML5  */
    add_theme_support( 'html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));

    /* Add default posts and comments RSS feed links to head. */
	add_theme_support( 'automatic-feed-links' );

    /*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
     */
	add_theme_support( 'title-tag' );


    /* Enable Support for WooCommerce */
    add_theme_support( 'woocommerce' );

    /* === REGISTER MENUS === */
    register_nav_menus(
        array(
          'primary' => esc_html__( 'Primary Menu', 'cocoon' ),
        )
    );
    
}

add_action('after_setup_theme', 'cocoon_setup');





/*
=====================================================
CHECK IF WC IS ACTIVE FUNCTION
=====================================================
*/

if (!function_exists( 'cocoon_is_woocommerce_activated' )) {
    function cocoon_wc_is_activated() {
        return class_exists('woocommerce') ? true : false;
    }
}





/*
=====================================================
COCOON CORE FUNCTION
=====================================================
*/

function cocoon_functions_config($Cocoon_Core) {
    $Cocoon_Core->offsetSet('themeName','Cocoon');
    $Cocoon_Core->offsetSet('themePath', get_stylesheet_directory());
    $Cocoon_Core->offsetSet('themeUri', get_stylesheet_directory_uri());
    $Cocoon_Core['Cocoon_AdminMenu'] = new Cocoon_AdminMenu($Cocoon_Core);
    $Cocoon_Core['Cocoon_Plugins'] = new Cocoon_Plugins($Cocoon_Core);
    $Cocoon_Core['Cocoon_Redirect'] = new Cocoon_Redirect($Cocoon_Core);
}

add_filter( 'cocoon/config', 'cocoon_functions_config', 10, 1 );


function cocoon_functions_core() {
    if( !class_exists('Cocoon_Core') ) {
    	$Cocoon_Core = array();
        global $Cocoon_Core;
        $Cocoon_Core['themeName'] = 'cocoon';
        $Cocoon_Core['themePath'] = get_stylesheet_directory();
        $start_menu = new Cocoon_AdminMenu($Cocoon_Core);
        $redirect = new Cocoon_Redirect($Cocoon_Core);
        $start_menu->run();
        $redirect->run();
        
        if ( function_exists('cocoon_stat_display') ) {
            $default_plugins = new Cocoon_Plugins($Cocoon_Core);
            $default_plugins->run();
        }
    }
}

add_action( 'init', 'cocoon_functions_core', 10, 1 );





/*
=====================================================
MAIN MENU FALLBACK
=====================================================
*/

function cocoon_menu_fallback( $output ) {
    if ( current_user_can('administrator') ) {
        echo( '
        <ul class="navbar-nav mx-auto" id="main-menu">
            <li class="nav-item"><a href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '">' . esc_html__('Add a Menu', 'cocoon') . '</a></li>
        </ul>' );
    } else {
        echo( '
        <ul class="navbar-nav mx-auto" id="main-menu">
            <li class="nav-item"></li>
        </ul>' );
    }
}





/*
=====================================================
    PAGING NAVIGATION
=====================================================
*/


/**
 * Navigation function for pagination.
 *
 * @since  1.0
 */
if ( ! function_exists( 'cocoon_paging_nav' ) ) {
    function cocoon_paging_nav() {
        cocoon_numeric_pagination();
    }
}





/**
 * Numeric Pagination
 *
 * @since  1.0.0
 */
if ( ! function_exists( 'cocoon_numeric_pagination' ) ) {
	function cocoon_numeric_pagination() {
		global $wp_query;

		if ( $wp_query->max_num_pages < 2 ) {
			return;
		}

		?>
        <div class="col-md-12 col-12">
            <nav class="pagination">
                <?php
                $big  = 999999999;
                $args = array(
                    'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'total'     => $wp_query->max_num_pages,
                    'current'   => max( 1, get_query_var( 'paged' ) ),
                    'prev_text' => esc_html__( 'Previous', 'cocoon' ),
                    'next_text' => esc_html__( 'Next', 'cocoon' ),
                    'type'      => 'list',
                );

                echo paginate_links( $args );
                ?>
            </nav>
        </div>
		<?php
	}
}




/*
=====================================================
    BLOG LOOP CUSTOM FUNCTION
=====================================================
*/

/**
 *  Meta informations for blog posts
 *
 * @since  1.0
 */
function cocoon_posted_meta() {
	echo '<div class="entry-meta">';

	if( is_single() ) {
	    $metas =  cocoon_get_option( 'cocoon_single_blog_meta' );

	    if (in_array("author", $metas)) {
	        echo '<span class="author"><i class="fa fa-keyboard-o"></i>';
	        echo esc_html__('By','cocoon') . ' <a class="author-link" rel="author" href="' . esc_url(get_author_posts_url(get_the_author_meta('ID' ))) . '">'; the_author_meta('display_name'); 
            echo '</a>';
	        echo '</span>';
	    }
	    if (in_array("date", $metas)) {
		    $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
			if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
				$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
			}

			$time_string = sprintf( $time_string,
				esc_attr( get_the_date( 'c' ) ),
				esc_html( get_the_date() ),
				esc_attr( get_the_modified_date( 'c' ) ),
				esc_html( get_the_modified_date() )
			);

		    echo '<span class="published"><i class="fa fa-clock-o"></i>'.$time_string.'</span>';

		}
	    if (in_array("cat", $metas)) {
	      if(has_category()) {
              echo '<span class="category"><i class="fa fa-folder-open-o"></i>';
              the_category(', ');
              echo '</span>';
          }
	    }
        if (in_array("tags", $metas)) {
	      if(has_tag()) {
              echo '<span class="tags"><i class="fa fa-tags"></i>'; the_tags('',', '); echo '</span>';
          }
	    }
	    if (in_array("com", $metas)) {
            echo '<span class="comments"><i class="fa fa-comment-o"></i>';
            comments_popup_link(
                esc_html__('0 comments','cocoon'),
                esc_html__('1 comment','cocoon'),
                esc_html__('% comments','cocoon'),
                'comments-link',
                esc_html__('Comments are off','cocoon')
            );
            echo '</span>';
	    }

  	} else {
	    $metas =  cocoon_get_option( 'cocoon_blog_meta' );

        if (in_array("author", $metas)) {
	        echo '<span class="author"><i class="fa fa-keyboard-o"></i>';
	        echo esc_html__('By','cocoon') . ' <a class="author-link" rel="author" href="' . esc_url(get_author_posts_url(get_the_author_meta('ID' ))) . '">'; the_author_meta('display_name');
            echo '</a>';
	        echo '</span>';
	    }
	    if (in_array("date", $metas)) {
		    $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
			if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
				$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
			}

			$time_string = sprintf( $time_string,
				esc_attr( get_the_date( 'c' ) ),
				esc_html( get_the_date() ),
				esc_attr( get_the_modified_date( 'c' ) ),
				esc_html( get_the_modified_date() )
			);

		    echo '<span class="published"><i class="fa fa-clock-o"></i>'.$time_string.'</span>';

		}
	    if (in_array("cat", $metas)) {
	      if(has_category()) {
              echo '<span class="category"><i class="fa fa-folder-open-o"></i>';
              the_category(', ');
              echo '</span>';
          }
	    }
        if (in_array("tags", $metas)) {
	      if(has_tag()) {
              echo '<span class="tags"><i class="fa fa-tags"></i>'; the_tags('',', '); echo '</span>';
          }
	    }
	    if (in_array("com", $metas)) {
            echo '<span class="comments"><i class="fa fa-comment-o"></i>';
            comments_popup_link(
                esc_html__('0 comments','cocoon'),
                esc_html__('1 comment','cocoon'),
                esc_html__('% comments','cocoon'),
                'comments-link',
                esc_html__('Comments are off','cocoon')
            );
            echo '</span>';
	    }

  	}
	echo "</div>";
}





/*
=====================================================
    SINGLE POST CUSTOM FUNCTIONS
=====================================================
*/



/**
 *  Display navigation to next/previous set of posts when applicable.
 *
 * @since  1.0
 */
function cocoon_get_post_navigation(){
    require_once( get_template_directory() . '/templates/extra/post-nav.php' );
}



/**
 *  Display comment navigation.
 *
 * @since  1.0
 */
function cocoon_get_comment_navigation(){
    if( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) {
		require_once( get_template_directory() . '/templates/comments/comment-nav.php' );
    }
}




/**
 *  Add support for Vertical Featured Images.
 *
 * @since  1.0
 */
if ( !function_exists('cocoon_vertical_featured_image') ) {
	function cocoon_vertical_featured_image( $html, $post_id, $post_thumbnail_id, $size, $attr ) {
		$image_data = wp_get_attachment_image_src( $post_thumbnail_id , 'large' );

		//Get the image width and height from the data provided by wp_get_attachment_image_src()
		$width  = $image_data[1];
		$height = $image_data[2];

		if ( $height > $width ) {
			$html = str_replace( 'attachment-', 'vertical-image attachment-', $html );
		}
		return $html;
	}

    add_filter( 'post_thumbnail_html', 'cocoon_vertical_featured_image', 10, 5 );
}





/*
=====================================================
    COMMENT FUNCTIONS
=====================================================
*/



/**
 * Comment Callback Function.
 *
 * @since 1.0.0
 */
if ( !function_exists('cocoon_comment') ) {
    require_once( get_template_directory() . '/templates/comments/comments.php' );
} // ends check for cocoon_comment()



/**
 * Comment Form.
 *
 * @since 1.0.0
 */
function cocoon_comment_form($args) {

	$commenter             = wp_get_current_commenter();
	$current_user          = wp_get_current_user();
	$req                   = get_option( 'require_name_email' );
	$aria_req              = ( $req ? " aria-required='true'" : '' );

	$comment_author        = esc_attr( $commenter['comment_author'] );
	$comment_author_email  = esc_attr( $commenter['comment_author_email'] );
	$comment_author_url    = esc_attr( $commenter['comment_author_url'] );

	$name                  = ($comment_author) ? '' : esc_html__('Name *','cocoon');
	$email                 = ($comment_author_email) ? '' : esc_html__('Email *','cocoon');
	$website               = ($comment_author_url) ? '' : esc_html__('Website','cocoon');

	$fields =  array(
	   'author' =>
            '<div class="col-md-6 form-group"><input id="author" name="author" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author'] ) . '" placeholder="' . esc_attr__('Your Name', 'cocoon') . '" required="required" /></div>',

        'email' =>
            '<div class="col-md-6 form-group"><input id="email" name="email" class="form-control" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" placeholder="' . esc_attr__('Your Email', 'cocoon') . '" required="required" /></div>'
	);

	$args = array(
        'class_form'            => esc_attr('row comment-form mb50'),
		'title_reply'           => esc_html__( 'Leave a Comment' , 'cocoon'),
        'title_reply_to'        => esc_html__('Leave a Reply to %s','cocoon'),
        'title_reply_before'    => '<h4 id="reply-title" class="comment-reply-title nomargin">',
		'title_reply_after'     => '</h4>',
        'submit_button'         => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',
        'submit_field'          => '<div class="col-12">%1$s %2$s</div>',
        'class_submit'          => esc_attr('btn btn-main'),
        'label_submit'          => esc_attr__( 'send comment', 'cocoon' ),
        'comment_field'         => '<div class="col-12 form-group"><textarea id="comment" class="form-control" name="comment" rows="8" placeholder="' . esc_attr__('Type your comment...', 'cocoon') . '" required="required"></textarea></div>',
        'comment_notes_before'  => '<div class="col-12"><p><em>' . esc_html__( 'Your email address will not be published.', 'cocoon' ) . '</em></p></div>',
        'logged_in_as'          => '<div class="col-12"><p class="logged-in-as">' . 
            sprintf(
                esc_html__( 'Logged in as ', 'cocoon' ) . '<a href="%1$s">%2$s</a>. <a href="%3$s" title="' . esc_html( 'Log out of this account', 'cocoon') . '">' . esc_html( 'Log out?', 'cocoon') . '</a>',
                esc_url(admin_url( 'profile.php' )), 
                $current_user->user_login, 
                wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) )
            ) . '</p></div>',       
		'cancel_reply_link'      => esc_html__('Cancel Reply','cocoon'),
		'fields'                 => apply_filters('comment_form_default_fields', $fields)
	);

    return $args;
    
}

add_filter('comment_form_defaults', 'cocoon_comment_form');





/*
=====================================================
    PAGE HEADER
=====================================================
*/

/**
 * Fetching the pages titles.
 *
 * @since 1.0.0
 */

if (!function_exists('cocoon_get_the_title')) {
    function cocoon_get_the_title() {

        if ( cocoon_wc_is_activated() ) {
            if (is_woocommerce()) {
                if (is_single() && !is_attachment()) {
                    echo get_the_title();
                } elseif (!is_single()) {
                    woocommerce_page_title();
                }

                return;
            }
        }

        if (is_404()) {
            return esc_html__('Error 404', 'cocoon');
        }

        // Homepage and Single Page
        if (is_home() || is_single() || is_404()) {
            return get_the_title();
        }

        // Search Page
        if (is_search()) {
            return sprintf( esc_html__( 'Search Results for: %s', 'cocoon' ), '<span>' . get_search_query() . '</span>' );
        }

        // Archive Pages
        if (is_archive()) {
            if (is_author()) {
                return sprintf( esc_html__( 'All posts by %s', 'cocoon'), get_the_author() );
            } elseif (is_day()) {
                return sprintf( esc_html__( 'Day: %s', 'cocoon'), get_the_date() );
            } elseif (is_month()) {
                return sprintf( esc_html__('Month: %s', 'cocoon'), get_the_date(_x('F Y', 'monthly archives date format', 'cocoon')) );
            } elseif (is_year()) {
                return sprintf( esc_html__('Year: %s', 'cocoon'), get_the_date(_x('Y', 'yearly archives date format', 'cocoon')) );
            } elseif (is_tag()) {
                return sprintf( esc_html__('Tag: %s', 'cocoon'), single_tag_title('', false) );
            } elseif (is_category()) {
                return sprintf( esc_html__('Category: %s', 'cocoon'), single_cat_title('', false) );
            } elseif (is_tax('post_format', 'post-format-aside')) {
                return esc_html__('Asides', 'cocoon');
            } elseif (is_tax('post_format', 'post-format-video')) {
                return esc_html__('Videos', 'cocoon');
            } elseif (is_tax('post_format', 'post-format-audio')) {
                return esc_html__('Audio', 'cocoon');
            } elseif (is_tax('post_format', 'post-format-quote')) {
                return esc_html__('Quotes', 'cocoon');
            } elseif (is_tax('post_format', 'post-format-gallery')) {
                return esc_html__('Galleries', 'cocoon');
            } else {
                return esc_html__('Archives', 'cocoon');
            }
        }

        return get_the_title();
    }
}





/*
=====================================================
    PHOTOSWIPE DIALOG ELEMENT
=====================================================
*/

function cocoon_product_images_lightbox() {

    if ( ! cocoon_wc_is_activated() ) {
        return;
    }

	if ( ! is_singular() ) {
		return;
	} ?>

	<div id="pswp" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

		<div class="pswp__bg"></div>

		<div class="pswp__scroll-wrap">

			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>

			<div class="pswp__ui pswp__ui--hidden">

				<div class="pswp__top-bar">


					<div class="pswp__counter"></div>

					<button class="pswp__button pswp__button--close" title="<?php esc_attr_e( 'Close (Esc)', 'cocoon' ) ?>"></button>

					<button class="pswp__button pswp__button--share" title="<?php esc_attr_e( 'Share', 'cocoon' ) ?>"></button>

					<button class="pswp__button pswp__button--fs" title="<?php esc_attr_e( 'Toggle fullscreen', 'cocoon' ) ?>"></button>

					<button class="pswp__button pswp__button--zoom" title="<?php esc_attr_e( 'Zoom in/out', 'cocoon' ) ?>"></button>

					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
							<div class="pswp__preloader__cut">
								<div class="pswp__preloader__donut"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
					<div class="pswp__share-tooltip"></div>
				</div>

				<button class="pswp__button pswp__button--arrow--left" title="<?php esc_attr_e( 'Previous (arrow left)', 'cocoon' ) ?>">
				</button>

				<button class="pswp__button pswp__button--arrow--right" title="<?php esc_attr_e( 'Next (arrow right)', 'cocoon' ) ?>">
				</button>

				<div class="pswp__caption">
					<div class="pswp__caption__center"></div>
				</div>

			</div>

		</div>

	</div>
	<?php
}

add_action( 'wp_footer', 'cocoon_product_images_lightbox' );





/*
=====================================================
    EXTRA FUNCTIONS
=====================================================
*/

/**
 *  Limits number of words from string.
 *
 * @since  1.0
 */

if ( ! function_exists( 'cocoon_string_limit_words' ) ) {
    function cocoon_string_limit_words($string, $word_limit) {
        $words = explode(' ', $string, ($word_limit + 1));
        if (count($words) > $word_limit) {
            array_pop($words);
            return implode(' ', $words) ;
        } else {
            return implode(' ', $words);
        }
    }
}





/**
 *  Remove attributes from Embeded Iframes
 *
 * @since  1.0
 */

function cocoon_oembed( $return, $data, $url ) {
    
    if ( is_object( $data ) &&  property_exists( $data, 'provider_name' ) &&  'Vimeo' === $data->provider_name || 'YouTube' === $data->provider_name || 'SoundCloud' === $data->provider_name ) {
        // Remove the unwanted attributes:
        $return = str_ireplace( array( 
            'frameborder="0"',
            'frameborder="no"',
            'webkitallowfullscreen', 
            'mozallowfullscreen',
            'scrolling="no"'
        ), '', $return );
    }
    
    return $return;
}

add_filter( 'oembed_dataparse', 'cocoon_oembed', 10, 3  );
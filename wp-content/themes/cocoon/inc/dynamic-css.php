<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* DYNAMIC CSS FILE- FOR ALL STYLES THAT ARE CREATED ON THE BACKEND
* ========================
*     
**/




function cocoon_stylesheet_content() {
    wp_add_inline_style( 'cocoon-main', cocoon_main_style() );    
}
add_action( 'wp_enqueue_scripts', 'cocoon_stylesheet_content' );




function cocoon_main_style() {
    $body_color         = cocoon_get_option( 'cocoon_body_color' );
    $wrapper_color      = cocoon_get_option( 'cocoon_wrapper_color' );
    $maincolor          = cocoon_get_option( 'cocoon_main_color' );
    $page_header_bg     = cocoon_get_option( 'cocoon_page_header_background' );
    
    $inline_css = '';
    
    /*** CSS Styling for the Logo ***/
	$logo_size_width = intval( cocoon_get_option( 'logo_width' ) );
	$logo_css        = $logo_size_width ? 'width:' . $logo_size_width . 'px; ' : '';

	$logo_size_height = intval( cocoon_get_option( 'logo_height' ) );
	$logo_css .= $logo_size_height ? 'height:' . $logo_size_height . 'px; ' : '';

	$logo_margin = cocoon_get_option( 'logo_margins' );
	$logo_css .= $logo_margin['top'] ? 'margin-top:' . $logo_margin['top'] . ' !important; ' : '';
	$logo_css .= $logo_margin['right'] ? 'margin-right:' . $logo_margin['right'] . ' !important; ' : '';
	$logo_css .= $logo_margin['bottom'] ? 'margin-bottom:' . $logo_margin['bottom'] . ' !important; ' : '';
	$logo_css .= $logo_margin['left'] ? 'margin-left:' . $logo_margin['left'] . ' !important; ' : '';

	if ( ! empty( $logo_css ) ) {
		$inline_css .= 'header .navbar-brand img, header .navbar h1.logo {' . esc_attr( $logo_css ) . ';}';
	}
    
    $inline_css .= 'body { background:' . esc_attr($body_color) . ' !important;}';
    $inline_css .= 'body > .wrapper { background:' . esc_attr($wrapper_color) . ' !important;}';
    
    
    /* ------------------------------------------------------------------- */
    /* Main Color: #22a3a4
    ---------------------------------------------------------------------- */
    /*** Color ***/
    $inline_css .= 'a, a:hover,
    .entry-meta > span i,
    blockquote:before,
    .btn-border.btn-main,
    header.header1 #main-nav .navbar-nav .dropdown-menu > li > a:hover,
    .woocommerce .products .product .price ins,
    .woocommerce .products .product .product-title a:hover,
    .woocommerce .products .product .product-btns-group > a:hover,
    .woocommerce .products .product .product-btns-group .yith-wcwl-add-to-wishlist .yith-wcwl-add-button a:hover:before,
    .quick-view-popup .product-summary .product-title a:hover,
    .quick-view-popup .owl-carousel.image-items:hover .owl-nav > button,
    .widget_tag_cloud .tagcloud a:hover, 
    .widget_product_tag_cloud .tagcloud a:hover,
    .woocommerce-cart .woocommerce .cart-collaterals .cart_totals table.shop_table .order-total strong,
    .woocommerce-checkout table.shop_table tr.cart-subtotal td span,
    .woocommerce-checkout table.shop_table tr.order-total td span,
    .woocommerce .woocommerce-order .woocommerce-table tfoot tr span.woocommerce-Price-amount,
    .woocommerce-account .woocommerce-MyAccount-navigation ul li.is-active a,
    #main-nav .navbar-nav .mega-menu .dropdown-menu .mega-menu-inner .menu-item-mega .sub-menu a:hover,
    .woocommerce div.product .cocoon-single-product-detail .price,
    .woocommerce .woocommerce-breadcrumb a:hover,
    section.page-header nav.breadcrumbs a:hover,
    .cocoon-off-canvas-panel .total .amount,
    header.header1 .general-search-wrapper .search-results ul li .search-item:hover,
    .cocoon-banners-grid ul .banner-item-text .link:hover,
    footer.footer1 .footer-widget-area .footer-widget a:hover,
    article.blog-post .blog-post-title a:hover,
    aside.widget a:hover,
    .entry-meta > span a:hover,
    section.comments .comment-form .logged-in-as a:hover {
        color:' . esc_attr($maincolor) . ';
    }';
    
    
     $inline_css .= '.quick-view-popup .product-summary .price {
        color:' . esc_attr($maincolor) . ' !important;
    }';
    
    
    /*** Background ***/
    $inline_css .= '.btn-main,
    .btn-border.btn-main:hover,
    .mm-counter,
    .owl-dots .owl-dot span,
    #backtotop,
    nav.pagination ul li span.current,
    article.blog-post-quote figure,
    header.header1 .extra-nav .menu-item-wishlist .notification-count,
    header.header1 .extra-nav .menu-item-cart .notification-count,
    .pricing-table.pricing-table-featured:before,
    .pricing-table2 .pricing-footer a,
    .cocoon-newsletter.nl-style-2 .nl-form input[type=submit],
    .woocommerce .products .product .product-thumb .button,
    #yith-wcwl-popup-message,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
    .woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content,
    .woocommerce table.wishlist_table .product-add-to-cart a,
    .woocommerce table.wishlist_table .product-add-to-cart a:hover,
    .woocommerce nav.woocommerce-pagination ul li span.current,
    .woocommerce #respond input#submit,
    .woocommerce #respond input#submit:hover,
    .cocoon-off-canvas-panel .buttons a,
    .cocoon-off-canvas-panel .buttons a:hover,
    .testimonial-cards .center .testimonial-message,
    .cocoon-banners-grid ul .banner-item-text h3,
    form.post-password-form input[type=submit],
    .cocoon-account-tabs .nav-pills .nav-link.active,
    .checkboxes input:checked~.checkmark {
        background:' . esc_attr($maincolor) . ';
    }';
    
    
    /*** Border Color ***/
    $inline_css .= '.btn-border.btn-main {
        border-color:' . esc_attr($maincolor) . ';
    }';
    
    
    $inline_css .= '#preloader [data-loader=circle-side] {
        border-left-color:' . esc_attr($maincolor) . ';
    }';
    
    
    
    
    /*** Page Header default Background Color ***/
    $inline_css .= '.page-header {
        background:' . esc_attr( $page_header_bg ) .';
    }';
    
    return $inline_css;
}
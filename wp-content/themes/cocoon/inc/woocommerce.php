<?php

/**
*
* @package Cocoon
*
* @since 1.0
*
* ========================
* WOOCOMMERCE MAIN FUNCTIONS
* ========================
*
**/


require get_template_directory() . '/inc/woocommerce/product-cat-header-image.php';


/*
==================================================================================
        GENERAL
==================================================================================
*/


/**
 *  WooCommerce Style Enqueue
 *
 * @since  1.0
 */
function cocoon_wc_style() {
    wp_enqueue_style( 'woocommerce', get_template_directory_uri() . '/assets/css/woocommerce.css');
}

add_action( 'wp_enqueue_scripts', 'cocoon_wc_style' );





/**
 * AJAXIFY the shopping page add to cart button.
 *
 * @since 1.0.0
 */
function cocoon_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
    ob_start();
    ?>

    <a class="nav-link cart" href="<?php echo esc_url( wc_get_cart_url() ); ?>">
        <i class="icon-bag"></i>
        <span class="notification-count">
            <?php echo esc_html(intval( $woocommerce->cart->cart_contents_count )); ?>
        </span>
    </a>

    <?php
    $fragments['a.cart'] = ob_get_clean();

    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'cocoon_add_to_cart_fragment');





/**
 * Ajaxify update count wishlist
 *
 * @since 1.0
 */
function cocoon_update_wishlist_count() {
    if ( ! function_exists( 'YITH_WCWL' ) ) {
        return;
    }

    wp_send_json( YITH_WCWL()->count_products() );

}

add_action( 'wp_ajax_cocoon_update_wishlist_count', 'cocoon_update_wishlist_count' );
add_action( 'wp_ajax_nopriv_cocoon_update_wishlist_count', 'cocoon_update_wishlist_count' );





/**
 * Product Quickview
 *
 * @since 1.0
 */

add_action( 'cocoon_quickview_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action( 'cocoon_quickview_single_product_summary', 'woocommerce_template_single_price', 20);
add_action( 'cocoon_quickview_single_product_summary', 'woocommerce_template_single_excerpt', 30);
add_action( 'cocoon_quickview_single_product_summary', 'woocommerce_template_single_add_to_cart', 40);

function cocoon_load_quickview_content_callback() {
    if( !isset($_POST['id']) ){
        die('0');
    }

    $prod_id = (int)($_POST['id']);

    global $post, $product;
    $post = get_post( $prod_id );
    $product = wc_get_product( $prod_id );

    if ( ! $product || ! $product->is_visible() ) {
        return;
    }

    $classes = 'cocoon-quickshop-wrapper product type-' . esc_attr( $product->get_type() );
    ob_start();
    ?>

    <div id="product-<?php echo esc_attr( get_the_ID() );?>" class="<?php echo esc_attr($classes); ?>">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 product-image">
                <div class="images-slider-wrapper">
                    <?php
                    $attachment_ids = $product->get_gallery_image_ids();
                    $attachment_count = count( $attachment_ids );

                    ?>
                    <div class="image-items">
                        <?php
                        $attributes = array(
                            'title' => esc_attr( get_the_title( get_post_thumbnail_id() ) )
                        );

                        if ( has_post_thumbnail() ) {

                            echo '<figure class="woocommerce-product-gallery__image">' . get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), $attributes ) . '</figure>';


                            if ( $attachment_count > 0 ) {
                                foreach ( $attachment_ids as $attachment_id ) {
                                    echo '<figure class="woocommerce-product-gallery__image">' . wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) ) . '</figure>';
                                }
                            }

                        } else {

                            echo '<figure class="woocommerce-product-gallery__image--placeholder">' . apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), esc_attr__( 'Placeholder', 'cocoon' ) ), $post->ID ) . '</figure>';

                        }

                        ?>
                    </div>

                </div>
            </div>

            <!-- Product summary -->
            <div class="col-md-6 col-sm-12 col-xs-12 product-summary">
                <div class="entry-summary">
                    <h1 class="product-title product-name">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </h1>

                    <?php do_action('cocoon_quickview_single_product_summary');

                    if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
                        echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
                    } ?>


                    <div class="product-meta">
                        <?php
                        do_action('woocommerce_product_meta_start');

                        if (wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))) {
                            echo '<div class="product-sku"><span>' . esc_html__('Sku: ', 'cocoon') . '</span><span class="sku">' . (($sku = $product->get_sku()) ? $sku : esc_html__('N/A', 'cocoon')) . '</span></div>';
                        }

                        echo wc_get_product_category_list($product->get_id(), ', ', '<div class="product-single-cats"><span>' . esc_html__('Categories:', 'cocoon') . '</span><span class="cat-links">', '</span></div>');

                        echo wc_get_product_tag_list($product->get_id(), ', ', '<div class="product-single-tags"><span>' . esc_html__('Tags:', 'cocoon') . '</span><span class="tag-links">', '</span></div>');

                        do_action('woocommerce_product_meta_end');
                        ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <?php
    $return = ob_get_clean();
    wp_reset_postdata();

    die($return);
}

/* Register ajax */
add_action('wp_ajax_load_quickshop_content', 'cocoon_load_quickview_content_callback' );
add_action('wp_ajax_nopriv_load_quickshop_content', 'cocoon_load_quickview_content_callback' );





/**
 * Add off canvas shopping cart to footer
 *
 * @since 1.0.0
 */

if ( ! function_exists( 'cocoon_off_canvas_cart' ) ) {
	function cocoon_off_canvas_cart() {

		if ( ! function_exists( 'woocommerce_mini_cart' ) ) {
			return;
		}

		$header_btns = cocoon_get_option('menu_extras');

		if ( empty( $header_btns ) || ! in_array( 'cart', $header_btns ) ) {
			return '';
		} ?>

		<div id="cart-panel" class="cart-panel woocommerce mini-cart cocoon-off-canvas-panel">
			<div class="widget-canvas-content">
				<div class="widget-cart-header widget-panel-header">
					<a href="#" class="close-canvas-panel"><span aria-hidden="true" class="icon-close"></span></a>
				</div>

				<div class="widget_shopping_cart_content">
					<?php woocommerce_mini_cart(); ?>
				</div>
			</div>
		</div>
        <div class="off-canvas-layer"></div>

		<?php
	}
}

add_action( 'wp_footer', 'cocoon_off_canvas_cart' );





/**
 * AJAX Search Products
 *
 * @since 1.0
 */

function cocoon_search_products() {
    check_ajax_referer( '_cocoon_nonce', 'nonce' );

    $args_sku = array(
        'post_type'         => 'product',
        'posts_per_page'    => 30,
        'meta_query'        => array(
            array(
                'key'           => '_sku',
                'value'         => trim( $_POST['term'] ),
                'compare'       => 'like',
            ),
        ),
        'suppress_filters'  => 0
    );

    $args_variation_sku = array(
        'post_type'         => 'product_variation',
        'posts_per_page'    => 30,
        'meta_query'        => array(
            array(
                'key'           => '_sku',
                'value'         => trim( $_POST['term'] ),
                'compare'       => 'like',
            ),
        ),
        'suppress_filters'  => 0
    );

    $args = array(
        'post_type'         => 'product',
        'posts_per_page'    => 30,
        's'                 => trim( $_POST['term'] ),
        'suppress_filters'  => 0
    );


    $products_sku           = get_posts( $args_sku );
    $products_s             = get_posts( $args );
    $products_variation_sku = get_posts( $args_variation_sku );

    $response    = array();
    $products    = array_merge( $products_sku, $products_s, $products_variation_sku );
    $product_ids = array();
    foreach ( $products as $product ) {
        $id = $product->ID;
        if ( ! in_array( $id, $product_ids ) ) {
            $product_ids[] = $id;

            $productw   = new WC_Product( $id );
            $response[] = sprintf(
                '<li>' .
                '<a class="search-item" href="%s">' .
                '%s' .
                '<span class="title">%s</span>' .
                '</a>' .
                '</li>',
                esc_url( $productw->get_permalink() ),
                $productw->get_image( 'shop_thumbnail' ),
                $productw->get_title()
            );
        }
    }


    if ( empty( $response ) ) {
        $response[] = sprintf( '<li>%s</li>', esc_html__( 'Nothing found', 'cocoon' ) );
    }

    $output = sprintf( '<ul>%s</ul>', implode( ' ', $response ) );

    wp_send_json_success( $output );
    die();
}

/* Register ajax */
add_action( 'wp_ajax_cocoon_search_products', 'cocoon_search_products' );
add_action( 'wp_ajax_nopriv_cocoon_search_products', 'cocoon_search_products' );





/**
* Move the Cross Sell under the table
*
* @since 1.0
*/
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' ); 
add_action( 'woocommerce_after_cart_table', 'woocommerce_cross_sell_display' );


/*
==================================================================================
        SHOP PRODUCTS & CATEGORIES
==================================================================================
*/


/**
 *  Remove WooCommerce Default Actions
 *
 * @since  1.0
 */
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);





/**
 * Add Bootstrap's column classes for product & product categories
 *
 * @since 1.0
 */
function cocoon_product_class( $classes, $class = '', $post_id = '' ) {

    if ( ! $post_id || get_post_type( $post_id ) !== 'product' || is_single( $post_id ) ) {
        return $classes;
    }

    global $woocommerce_loop;


    if ( ! is_search() ) {
        $classes[] = 'col-md-' . ( 12 / $woocommerce_loop['columns'] );
        $classes[] = ' col-sm-6 col-12';
    } else {
        if ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
            $classes[] = 'col-md-' . ( 12 / $woocommerce_loop['columns'] );
            $classes[] = ' col-sm-6 col-12';
        }
    }

    return $classes;
}


function cocoon_product_cat_class( $classes, $class = '', $category = '' ) {

    if ( is_search() ) {
        return $classes;
    }

    global $woocommerce_loop;

    $classes[] = 'col-md-' . ( 12 / $woocommerce_loop['columns'] );
    $classes[] = ' col-sm-6 col-12';

    return $classes;
}

add_filter( 'post_class', 'cocoon_product_class', 30, 3 );
add_filter( 'product_cat_class', 'cocoon_product_cat_class', 30, 3 );





/**
 * Add Custom Product Thumbnail
 *
 * @since 1.0
 */

function cocoon_wc_template_loop_product_thumb() {
    global $product;

    $alt_thumb = $product->get_gallery_image_ids();
    $output = '<div class="product-thumb">';
    $output .= '<a href="' . esc_url( get_the_permalink() ) . '" class="loop-thumbnail">';

    if ( has_post_thumbnail() ) {
        $output .= woocommerce_get_product_thumbnail();
    } else {
        $output .= wc_placeholder_img();
    }

    if ( cocoon_get_option('cocoon_secondary_thumb') == 1 ) {
        if (!empty($alt_thumb)) {
            $output .= '<figure class="image-switcher">';
            $output .= wp_get_attachment_image($alt_thumb[0], 'shop_single');
            $output .= '</figure>';
        }
    }

    $output .= '</a>';

    return $output;
}

function cocoon_wc_template_loop_product_thumb_output() {
    echo cocoon_wc_template_loop_product_thumb();
}

function cocoon_wc_template_loop_product_thumb_close() {
    $output = '</div>';

    return $output;
}

function cocoon_wc_template_loop_product_thumb_close_output() {
    echo cocoon_wc_template_loop_product_thumb_close();
}

add_action( 'woocommerce_before_shop_loop_item', 'cocoon_wc_template_loop_product_thumb_output', 10 );
add_action( 'woocommerce_before_shop_loop_item', 'cocoon_wc_template_loop_product_thumb_close_output', 20 );





/**
 * Add Custom Product Category
 *
 * @since 1.0
 */

function cocoon_template_loop_product_categories() {
    global $product;

    $output = '<div class="product-categories">';
    $output .= wc_get_product_category_list($product->get_id(),', ');
    $output .= '</div>';

    return $output;
}

function cocoon_template_loop_product_categories_output() {
    $categories = cocoon_get_option('cocoon_product_category');
    
    if ( $categories == 1 ) {
        echo cocoon_template_loop_product_categories();
    }
    
}

add_action( 'woocommerce_after_shop_loop_item', 'cocoon_template_loop_product_categories_output', 10);





/**
 * Add Custom Product Title
 *
 * @since 1.0
 */

function cocoon_template_loop_product_title() {
    global $post, $product;
    $product_url = get_permalink($post->ID);

    $output = '<h3 class="product-title product-name">';
    $output .= '<a href=' . esc_url($product_url) . '>' . esc_html(get_the_title()) . '</a>';
    $output .= '</h3>';

    return $output;
}

function cocoon_template_loop_product_title_output() {
    echo cocoon_template_loop_product_title();
}

add_action( 'woocommerce_after_shop_loop_item', 'cocoon_template_loop_product_title_output', 10);
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_rating', 11);
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 12);





/**
 * Display badge for new product or featured product
 *
 * @since 1.0
 */

function cocoon_product_label() {
    global $product, $post;

    $output = '';

    if ( cocoon_get_option('cocoon_show_badges') == 1) {

        $badges = cocoon_get_option('cocoon_badges');
        $out_of_stock = false;

        if( !$product->is_in_stock() && !is_product() ){
            $out_of_stock = true;
        }

        $output .= '<div class="product-labels">';

        if ($product->is_on_sale() && !$out_of_stock) {

            $percentage = '';

            if ( $product->get_type() == 'variable' ) {

                $available_variations = $product->get_variation_prices();
                $max_percentage = 0;

                foreach( $available_variations['regular_price'] as $key => $regular_price ) {
                    $sale_price = $available_variations['sale_price'][$key];

                    if ( $sale_price < $regular_price ) {
                        $percentage = round( ( ( $regular_price - $sale_price ) / $regular_price ) * 100 );

                        if ( $percentage > $max_percentage ) {
                            $max_percentage = $percentage;
                        }
                    }
                }

                $percentage = $max_percentage;

            } elseif ( $product->get_type() == 'simple' || $product->get_type() == 'external' ) {
                    $percentage = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
            }

            if (in_array('sale', $badges)) {
                if ( $percentage ) {
                    $output .= '<span class="onsale percent">-' . esc_html($percentage) . '%' . '</span>';
                } else {
                    $output .= '<span class="onsale">' . esc_html__( 'Sale', 'cocoon' ) . '</span>';
                }
            }
        }

        /* Hot label */
        if ($product->is_featured() && !$out_of_stock && in_array('hot', $badges)) {
            $output .= '<span class="featured">' . esc_html(cocoon_get_option('cocoon_hot_text')) . '</span>';
        }

        /* Out of stock */
        if ($out_of_stock && in_array('outofstock', $badges)) {
            $output .= '<span class="out-of-stock">' . esc_html(cocoon_get_option('cocoon_outofstock_text')) . '</span>';
        }


        $output .= '</div>';
    }

    return $output;
}

function cocoon_product_label_output() {
    echo cocoon_product_label();
}

add_action('woocommerce_before_shop_loop_item_title', 'cocoon_product_label_output', 1);





/**
 * Add Product Buttons
 *
 * @since 1.0
 */
function cocoon_product_group_button() {
    global $product;

    $output = '<div class="product-btns-group" >';


    if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
        $output .= do_shortcode( '[yith_wcwl_add_to_wishlist]' );
    }

    if ( cocoon_get_option('cocoon_product_quick_view') == 1 ) {
        $output .= '<a href="#quickview" class="product-quickview" data-id="' . esc_attr( $product->get_id() ) . '" data-toggle="tooltip" data-placement="left" title="' . esc_attr__( 'Quick View', 'cocoon' ) . '"><i class="icon-magnifier"></i></a>';
    }


    $output .= '</div>';

    return $output;
}


function cocoon_product_group_button_output() {
    echo cocoon_product_group_button();
}

add_action('woocommerce_before_shop_loop_item', 'cocoon_product_group_button_output', 15);
add_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 15);





/**
 *  Add Grid & List view buttons
 *
 * @since  1.0
 */
function cocoon_gridlist_toggle_button() { ?>

    <nav class="grid_list_nav">
        <a href="#" id="grid" title="<?php esc_attr_e('Grid view', 'cocoon');?>" class="active"><i class="icon-grid"></i></a>
        <a href="#" id="list" title="<?php esc_attr_e('List view', 'cocoon'); ?>"><i class="icon-list"></i></a>
    </nav>

    <?php
}

add_action( 'woocommerce_before_shop_loop', 'cocoon_gridlist_toggle_button', 30);





/**
 *  Add Short Description
 *
 * @since  1.0
 */
function cocoon_product_short_description() {
    global $product, $post;

    if (empty($post->post_excerpt)) {
        return;
    }

    $is_archive = is_tax(get_object_taxonomies('product')) || is_post_type_archive('product');
    $length     = intval( cocoon_get_option( 'shop_expert_length' ) );

    if ( $is_archive) { ?>
        <div class="short-description" style="display: none;">
            <?php echo cocoon_string_limit_words( get_the_excerpt(), $length ); ?>...
        </div>
    <?php
    }
}

add_action('woocommerce_after_shop_loop_item', 'cocoon_product_short_description', 20);





/*
==================================================================================
        SINGLE PRODUCT
==================================================================================
*/


/**
 *  Remove WooCommerce Default Actions
 *
 * @since  1.0
 */
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);





/**
 * Remove Sidebar from Single Product Page
 *
 * @since 1.0.0
 */
function cocoon_remove_sidebar_product_pages() {
    if ( is_product() ) {
        remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
    }
}

add_action( 'wp', 'cocoon_remove_sidebar_product_pages' );





/**
 * Single Product Meta Info
 *
 * @since 1.0.0
 */
function cocoon_template_single_product_meta() {
    global $product;

    echo '<div class="product-meta">';

    do_action('woocommerce_product_meta_start');

    if (wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))) {
        echo '<div class="product-sku"><span>' . esc_html__('Sku: ', 'cocoon') . '</span><span class="sku">' . (($sku = $product->get_sku()) ? $sku : esc_html__('N/A', 'cocoon')) . '</span></div>';
    }

    echo wc_get_product_category_list($product->get_id(), ', ', '<div class="product-single-cats"><span>' . esc_html__('Categories:', 'cocoon') . '</span><span class="cat-links">', '</span></div>');

    echo wc_get_product_tag_list($product->get_id(), ', ', '<div class="product-single-tags"><span>' . esc_html__('Tags:', 'cocoon') . '</span><span class="tag-links">', '</span></div>');

    do_action('woocommerce_product_meta_end');

    echo '</div>';
}

add_action( 'woocommerce_single_product_summary', 'cocoon_template_single_product_meta', 40);





/**
 * Single Product Share
 *
 * @since 1.0.0
 */
function cocoon_template_single_product_share() {
    global $product;

    echo '<div class="product-share">';

    if ( function_exists( 'cocoon_share_media' ) ) {
        echo cocoon_share_media();
    }


    echo '</div>';
}

add_action( 'woocommerce_single_product_summary', 'cocoon_template_single_product_share', 40);





/**
 * Single Product Navigation
 *
 * @since 1.0.0
 */
function cocoon_template_single_navigation() {
    $next = get_next_post();
    $prev = get_previous_post();

    $next = (!empty($next)) ? wc_get_product($next->ID) : false;
    $prev = (!empty($prev)) ? wc_get_product($prev->ID) : false;
    ?>
   <div class="cocoon-product-nav">
    <?php if (!empty($prev)): ?>
        <a href="<?php echo esc_url($prev->get_permalink()); ?>" class="prev">
            <div class="nav-product prev-product">
                <div class="product-image">
                    <?php echo wp_kses($prev->get_image(), array('img' => array('class' => true, 'width' => true, 'height' => true, 'src' => true, 'alt' => true, 'data-src' => true))); ?>
                </div>
                <div class="product-description">
                    <span class="product-title"><?php echo esc_html($prev->get_title()); ?></span>
                    <span class="price"><?php echo wp_kses_post($prev->get_price_html()); ?></span>
                </div>
            </div>
        </a>
    <?php endif;?>
    <?php if (!empty($next)): ?>
        <a href="<?php echo esc_url($next->get_permalink()); ?>" class="next">
            <div class="nav-product next-product">
                <div class="product-image">
                    <?php echo wp_kses($next->get_image(), array('img' => array('class' => true, 'width' => true, 'height' => true, 'src' => true, 'alt' => true, 'data-src' => true))); ?>
                </div>
                <div class="product-description">
                    <span class="product-title"><?php echo esc_html($next->get_title()); ?></span>
                    <span class="price"><?php echo wp_kses_post($next->get_price_html()); ?></span>
                </div>
            </div>
        </a>
    <?php endif;?>
</div>
<?php
}





/**
 * Remove Features on the Single Product Page
 *
 * @since 1.0.0
 */
function cocoon_remove_single_product_features() {

    // Remove Ralate Products
    if ( cocoon_get_option( 'product_related' ) == 0 ) {
        remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
    }

    // Remove Up-sell Products
    if ( cocoon_get_option( 'product_upsells' ) == 0 ) {
        remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
    }

    // Remove Sharing Buttons
    if ( cocoon_get_option( 'product_share' ) == 0 ) {
        remove_action( 'woocommerce_single_product_summary', 'cocoon_template_single_product_share', 40);
    }
}

add_action( 'wp', 'cocoon_remove_single_product_features' );





/**
 * Display product stock
 *
 * @since 1.0
 */
function cocoon_product_stock_html( $availability_html ) {
    global $product;

    $availability      = $product->get_availability();

    $availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html__( 'Available', 'cocoon' ) . ': <span>' . esc_html( $availability['availability'] ) . '</span></p>';

    return $availability_html;
}

add_filter( 'woocommerce_get_stock_html', 'cocoon_product_stock_html' );





/**
 * Product video button
 *
 * @since  1.0.0
 */
function cocoon_single_product_video_button() {
    global $post;

    $video_url = get_post_meta( $post->ID, 'cocoon_product_video_url', true);
    if ( !empty($video_url) ) {
        echo '<a href="' . esc_url($video_url) . '" class="popup-video"><span class="play-video"><span class="fa fa-play"></span></span></a>';
    }
}

add_filter( 'cocoon_after_single_product_image', 'cocoon_single_product_video_button', 50 );





/**
 * Get product thumbnails
 *
 * @since  1.0.0
 */
function cocoon_product_thumbnails() {
    global $post, $product, $woocommerce;

    $attachment_ids = $product->get_gallery_image_ids();

    if ( $attachment_ids ) {
        $loop    = 1;
        $columns = apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
        ?>
        <div class="product-thumbnails" id="product-thumbnails">
            <div class="thumbnails <?php echo 'columns-' . esc_attr($columns); ?>"><?php

                $image_thumb = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );

                if ( $image_thumb ) {
                    printf(
                        '<div>%s</div>',
                        $image_thumb
                    );
                }

                if ( $attachment_ids ) {
                    foreach ( $attachment_ids as $attachment_id ) {

                        echo apply_filters(
                            'woocommerce_single_product_image_thumbnail_html',
                            sprintf(
                                '<div>%s</div>',
                                wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0 )
                            ),
                            $attachment_id,
                            $post->ID
                        );

                        $loop ++;
                    }
                }
                ?>
            </div>
        </div>
        <?php
    }
}

add_filter( 'cocoon_after_single_product_image', 'cocoon_product_thumbnails' );





/**
 * Get product size guide
 *
 * @since  1.0
 *
 * @return string
 */

if ( ! function_exists( 'cocoon_product_size_guide' ) ) {
	function  cocoon_product_size_guide() {
		global $product;

		echo get_post_meta( $product->get_id(), 'cocoon_product_size_guide', true );
	}
}





/**
 * Add Product Size Guide Tab
 *
 * @since  1.0.0
 */
function cocoon_product_size_guide_tab( $args ) {
    global $product;

    $product_custom_tab1 = cocoon_get_option( 'custom_product_tab_1' );
    $size_guide          = get_post_meta( $product->get_id(), 'cocoon_product_size_guide', true );

    if ( $size_guide ) {
        $args['size_guide'] = array(
            'title'    => $product_custom_tab1,
            'priority' => 21,
            'callback' => 'cocoon_product_size_guide'
        );
    }

    return $args;
}

add_filter( 'woocommerce_product_tabs', 'cocoon_product_size_guide_tab' );





/**
 * Get product shipping
 *
 * @since  1.0
 *
 * @return string
 */
if ( ! function_exists( 'cocoon_product_shipping' ) ) {
	function  cocoon_product_shipping() {
		global $product;

		echo get_post_meta( $product->get_id(), 'cocoon_product_shipping', true );
	}
}





/**
 * Add Product Shipping Tab
 *
 * @since  1.0.0
 */
function cocoon_product_shipping_tab( $args ) {
    global $product;

    $product_custom_tab2 = cocoon_get_option( 'custom_product_tab_2' );
    $shipping            = get_post_meta( $product->get_id(), 'cocoon_product_shipping', true );

    if ( $shipping ) {
        $args['shipping'] = array(
            'title'    => $product_custom_tab2,
            'priority' => 22,
            'callback' => 'cocoon_product_shipping'
        );
    }

    return $args;
}

add_filter( 'woocommerce_product_tabs', 'cocoon_product_shipping_tab' );

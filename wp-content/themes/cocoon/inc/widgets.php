<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* WIDGET OPTIONS
* ========================
*     
**/




/* 
=====================================================
REGISTER WIDGET AREAS
=====================================================
*/

function cocoon_widgets_areas() {
    
    
    register_sidebar( array(
        'id'              => 'sidebar-1',
        'name'            => esc_html__( 'Sidebar', 'cocoon' ),
        'description'     => esc_html__('The primary widget area', 'cocoon'),
        'before_widget'   => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'    => '</aside>',
        'before_title'    => '<h4 class="widget-title">',
        'after_title'     => '</h4>',
    ) );
    
    register_sidebar( array(
        'id'              => 'sidebar-shop',
        'name'            => esc_html__( 'Shop - Sidebar', 'cocoon' ),
        'description'     => esc_html__('The shop sidebar widget area', 'cocoon'),
        'before_widget'   => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'    => '</aside>',
        'before_title'    => '<h4 class="widget-title">',
        'after_title'     => '</h4>',
    ) );

    for ( $i = 1; $i <= 4; $i++ ) {
        register_sidebar( array(
            'name'          => sprintf( esc_html__( 'Footer Widget Area Column %d', 'cocoon' ), absint( $i ) ),
            'id'            => 'footer-widget-area' . ( $i > 1 ? ( '-' . absint( $i ) ) : '' ),
            'description'   => esc_html__( 'Choose what should display in this footer widget column.', 'cocoon' ),
            'before_widget' => '<aside id="%1$s" class="footer-widget widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title widget-title-footer">',
            'after_title'   => '</h4>',
        ) );
    }

}

add_action( 'widgets_init', 'cocoon_widgets_areas' );
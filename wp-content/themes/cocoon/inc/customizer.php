<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* CUSTOMIZER OPTIONS
* ========================
*     
**/




/** Exit if accessed directly **/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class cocoon_Customize {
    
	/** Customize settings */
	protected $config = array();

    
	/** The class constructor */
	public function __construct( $config ) {
		$this->config = $config;

		if ( ! class_exists( 'Kirki' ) ) {
			return;
		}

		$this->register();
	}

	/** Register settings **/
	public function register() {

		/** Add the theme configuration **/
		if ( ! empty( $this->config['theme'] ) ) {
			Kirki::add_config(
				$this->config['theme'], array(
					'capability'  => 'edit_theme_options',
					'option_type' => 'theme_mod',
				)
			);
		}

		/** Add panels **/
		if ( ! empty( $this->config['panels'] ) ) {
			foreach ( $this->config['panels'] as $panel => $settings ) {
				Kirki::add_panel( $panel, $settings );
			}
		}

		/**  Add sections **/
		if ( ! empty( $this->config['sections'] ) ) {
			foreach ( $this->config['sections'] as $section => $settings ) {
				Kirki::add_section( $section, $settings );
			}
		}

		/** Add fields **/
		if ( ! empty( $this->config['theme'] ) && ! empty( $this->config['fields'] ) ) {
			foreach ( $this->config['fields'] as $name => $settings ) {
				if ( ! isset( $settings['settings'] ) ) {
					$settings['settings'] = $name;
				}

				Kirki::add_field( $this->config['theme'], $settings );
			}
		}
	}
    
	/** Get config ID **/
	public function get_theme() {
		return $this->config['theme'];
	}    
    
	/** Get customize setting value **/
	public function get_option( $name ) {
		if ( ! isset( $this->config['fields'][$name] ) ) {
			return false;
		}

		$default = isset( $this->config['fields'][$name]['default'] ) ? $this->config['fields'][$name]['default'] : false;

		return get_theme_mod( $name, $default );
	}
}




/*** Move some default sections to 'general' panel ***/
function cocoon_customize_modify( $wp_customize ) {
	$wp_customize->get_section( 'title_tagline' )->panel     = 'general';
	$wp_customize->get_section( 'static_front_page' )->panel = 'general';
}

add_action( 'customize_register', 'cocoon_customize_modify' );




/** This is a short hand function for getting setting value from customizer **/
function cocoon_get_option( $name ) {
	global $cocoon_customize;

	$value = false;

	if ( class_exists( 'Kirki' ) ) {
		$value = Kirki::get_option( 'cocoon', $name );
	} elseif ( ! empty( $cocoon_customize ) ) {
		$value = $cocoon_customize->get_option( $name );
	}

	return apply_filters( 'cocoon_get_option', $value, $name );
}


/** Get customize settings **/
function cocoon_customize_settings() {
    
	/** Customizer configuration **/
	return array(
        'theme'    => 'cocoon',

        'panels'   => array(
            
            //  GENERAL OPTIONS
            'general'  => array(
                'priority'    => 10,
                'title'       => esc_html__( 'General Options', 'cocoon' ),
                'description' => esc_html__( 'General options', 'cocoon' ),
            ),
            
            //  TYPOGRAPHY GENERAL OPTIONS
            'typo_general'  => array(
                'priority'    => 10,
                'title'       => esc_html__( 'Typography Options', 'cocoon' ),
                'description' => esc_html__( 'Typography related options', 'cocoon' ),
            ),

            //  HEADER GENERAL OPTIONS
            'header_general'  => array(
                'priority'    => 10,
                'title'       => esc_html__( 'Header Options', 'cocoon' ),
                'description' => esc_html__( 'Header related options', 'cocoon' ),
            ),
            
            //  WOOCOMMERCE GENERAL OPTIONS
            'woocommerce' => array(
				'priority'    => 11,
				'title'       => esc_html__( 'WooCommerce', 'cocoon' ),
                'description' => esc_html__( 'WooCommerce related options', 'cocoon' ),
			),
            
            //  BLOG OPTIONS
            'blog_general'    => array(
                'priority'    => 11,
                'title'       => esc_html__( 'Blog Options', 'cocoon' ),
                'description' => esc_html__( 'Blog related options', 'cocoon' ),
            ),
            
            //  PAGES OPTIONS
            'pages_general'    => array(
                'priority'    => 11,
                'title'       => esc_html__( 'Pages Options', 'cocoon' ),
                'description' => esc_html__( 'Pages related options', 'cocoon' ),
            ),

            
        ), // end of panels array


        
        'sections' => array(
            
            //  COLORS OPTIONS
            'colors'             => array(
                'title'          => esc_html__( 'Color Options', 'cocoon'  ),
                'description'    => '',
                'panel'          => 'general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            //  PAGE HEADER OPTIONS
            'page_header'             => array(
                'title'          => esc_html__( 'Page Header Options', 'cocoon'  ),
                'description'    => '',
                'panel'          => 'general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            //  EXTRA OPTIONS
            'extra_options'             => array(
                'title'          => esc_html__( 'Extra Options', 'cocoon'  ),
                'description'    => '',
                'panel'          => 'general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            //  BODY - TYPOGRAPHY OPTIONS
            'body_typo'             => array(
                'title'          => esc_html__( 'Body', 'cocoon'  ),
                'description'    => '',
                'panel'          => 'typo_general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            //  HEADINGS - TYPOGRAPHY OPTIONS
            'headings_typo'             => array(
                'title'          => esc_html__( 'Heading', 'cocoon'  ),
                'description'    => '',
                'panel'          => 'typo_general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            //  MENU - TYPOGRAPHY OPTIONS
            'menu_typo'             => array(
                'title'          => esc_html__( 'Menu', 'cocoon'  ),
                'description'    => '',
                'panel'          => 'typo_general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            // LOGO OPTIONS
            'logo'         => array(
                'title'          => esc_html__( 'Logo', 'cocoon'  ),
                'description'    => '',
                'panel'          => 'header_general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),

            // MAIN HEADER OPTIONS
            'header'             => array(
                'title'          => esc_html__( 'Header Options', 'cocoon'  ),
                'description'    => esc_html__( 'Header related options', 'cocoon'  ),
                'panel'          => 'header_general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            //  FOOTER OPTIONS
            'footer' => array(
                'title'          => esc_html__( 'Footer Options', 'cocoon'  ),
                'description'    => esc_html__( 'Footer related options', 'cocoon'  ),
                'panel'          => '', // Not typically needed.
                'priority'       => 10,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            // GENERAL BLOG OPTIONS
            'blog'               => array(
                'title'          => esc_html__( 'General Blog Options', 'cocoon'  ),
                'description'    => esc_html__( 'Blog related options', 'cocoon'  ),
                'panel'          => 'blog_general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),

            //SINGLE BLOG POST OPTIONS
            'single_blog_post'   => array(
                'title'          => esc_html__( 'Single Blog Post Options', 'cocoon'  ),
                'description'    => esc_html__( 'Single blog post related options', 'cocoon'  ),
                'panel'          => 'blog_general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            //SEARCH PAGE OPTIONS
            'search_page'        => array(
                'title'          => esc_html__( 'Search Page Options', 'cocoon'  ),
                'description'    => esc_html__( 'Search page related options', 'cocoon'  ),
                'panel'          => 'pages_general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            //404 PAGE OPTIONS
            'page_not_found'        => array(
                'title'          => esc_html__( '404 Page Options', 'cocoon'  ),
                'description'    => esc_html__( '404 page related options', 'cocoon'  ),
                'panel'          => 'pages_general', // Not typically needed.
                'priority'       => 160,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            //MAP OPTIONS
            'map_options'   => array(
                'title'          => esc_html__( 'Map Options', 'cocoon'  ),
                'description'    => esc_html__( 'Map related options', 'cocoon'  ),
                'panel'          => '', // Not typically needed.
                'priority'       => 12,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ),
            
            // GENERAL SHOP LAYOUT
            'shop_layout'  => array(
				'title'       => esc_html__( 'Shop Layout', 'cocoon' ),
				'description' => '',
				'priority'    => 5,
				'panel'       => 'woocommerce',
				'capability'  => 'edit_theme_options',
			),
            
            // SHOP BADGES
            'shop_badges'  => array(
				'title'       => esc_html__( 'Badges', 'cocoon' ),
				'description' => '',
				'priority'    => 50,
				'panel'       => 'woocommerce',
				'capability'  => 'edit_theme_options',
			),
            
            // PRODUCT GRID
            'product_grid'  => array(
				'title'       => esc_html__( 'Product Grid', 'cocoon' ),
				'description' => '',
				'priority'    => 50,
				'panel'       => 'woocommerce',
				'capability'  => 'edit_theme_options',
			),
            
            // PRODUCT GRID
            'single_product'  => array(
				'title'       => esc_html__( 'Single Product', 'cocoon' ),
				'description' => '',
				'priority'    => 50,
				'panel'       => 'woocommerce',
				'capability'  => 'edit_theme_options',
			),
           
        ), // end of sections array
        
        
        
        'fields'   => array(
            
            //  GENERAL - COLORS OPTIONS
            'cocoon_body_color' => array(
                'type'        => 'color',
                'label'       => esc_html__( 'Select body color', 'cocoon' ),
                'description' => '',
                'section'     => 'colors',
                'default'     => '#fff',
                'priority'    => 10,
            ),
            
            'cocoon_wrapper_color' => array(
                'type'        => 'color',
                'label'       => esc_html__( 'Select body wrapper color', 'cocoon' ),
                'description' => '',
                'section'     => 'colors',
                'default'     => '#fff',
                'priority'    => 10,
            ),
            
            'cocoon_main_color' => array(
                'type'        => 'color',
                'label'       => esc_html__( 'Select main theme color', 'cocoon' ),
                'description' => '',
                'section'     => 'colors',
                'default'     => '#22a3a4',
                'priority'    => 10,
            ),
            
            //  PAGE HEADER OPTIONS
            'cocoon_page_header_background' => array(
                'type'        => 'color',
                'label'       => esc_html__( 'Select the default background color for your page header', 'cocoon' ),
                'description' => '',
                'section'     => 'page_header',
                'default'     => '#252525',
                'priority'    => 10,
            ),
            
            'cocoon_page_header_color_overlay' => array(
                'type'        => 'color',
                'label'       => esc_html__( 'Select color overlay for your page header', 'cocoon' ),
                'description' => '',
                'section'     => 'page_header',
                'default'     => '#252525',
                'priority'    => 10,
            ),
            
            'cocoon_page_header_overlay_opacity' => array(
                'type'        => 'slider',
                'label'       => esc_html__( 'Select the opacity for the color overlay', 'cocoon' ),
                'description' => '',
                'section'     => 'page_header',
                'default'     => '0.85',
                'priority'    => 10,
                'choices'     => array(
                    'min'  => '0',
                    'max'  => '1',
                    'step' => '0.01',
                ),
            ),
            
            'cocoon_general_breadcrumbs' => array(
                'type'        => 'switch',
                'label'       => esc_html__( 'Breadcrumbs', 'cocoon' ),
                'description' => esc_html__( 'Turn the switch "OFF" to disable the breadcrumbs.', 'cocoon' ),
                'section'     => 'page_header',
                'default'     => true,
                'priority'    => 10,
            ),
            
            //  EXTRA OPTIONS
            'cocoon_body_style'         => array(
                'type'        => 'select',
                'label'       => esc_html__( 'Layout style', 'cocoon' ),
                'description' => '',
                'section'     => 'extra_options',
                'default'     => 'fullwidth',
                'priority'    => 10,
                'choices'     => array(
                    'fullwidth' => esc_html__( 'Full-Width', 'cocoon' ),
                    'boxed'		=> esc_html__( 'Boxed', 'cocoon' ),
                ),
            ),
            
            'cocoon_preloader'          => array(
                'type'        => 'switch',
                'label'       => esc_html__( 'Page Preloader', 'cocoon' ),
                'description' => esc_html__( 'Turn the switch "OFF" to disable the preload animation.', 'cocoon' ),
                'section'     => 'extra_options',
                'default'     => true,
                'priority'    => 10,
            ),
            
            'cocoon_back_top'           => array(
                'type'        => 'switch',
                'label'       => esc_html__( 'Back To Top', 'cocoon' ),
                'description' => esc_html__( 'Turn the switch "OFF" to disable the back to top button.', 'cocoon' ),
                'section'     => 'extra_options',
                'default'     => true,
                'priority'    => 10,
            ),
            
            
            //  BODY - TYPOGRAPHY OPTIONS
            'cocoon_body_typo' => array(
                'type'        => 'typography',
                'label'       => esc_html__( 'Body Typography', 'cocoon' ),
                'description' => '',
                'section'     => 'body_typo',
                'priority'    => 10,
                'default'  => array(
					'font-family'    => 'Poppins',
					'variant'        => 'regular',
					'font-size'      => '16px',
					'line-height'    => '1.65',
					'letter-spacing' => '0',
					'subsets'        => '',
					'color'          => '#948a99',
					'text-transform' => 'none',
				),
                'output'   => array(
					array(
						'element' => 'body',
					),
				),
            ),
            
            //  HEADINGS - TYPOGRAPHY OPTIONS
            
            'cocoon_heading1_typo' => array(
				'type'     => 'typography',
				'label'    => esc_html__( 'Heading 1', 'cocoon' ),
				'section'  => 'headings_typo',
				'priority' => 10,
				'default'  => array(
					'font-family'    => 'Poppins',
					'variant'        => '500',
					'font-size'      => '46px',
					'line-height'    => '1.3',
					'letter-spacing' => '0',
					'subsets'        => '',
					'color'          => '#1f1f1f',
					'text-transform' => 'none',
				),
				'output'   => array(
					array(
						'element' => 'h1',
					),
				),
			),
            
            'cocoon_heading2_typo' => array(
				'type'     => 'typography',
				'label'    => esc_html__( 'Heading 2', 'cocoon' ),
				'section'  => 'headings_typo',
				'priority' => 10,
				'default'  => array(
					'font-family'    => 'Poppins',
					'variant'        => '500',
					'font-size'      => '38px',
					'line-height'    => '1.3',
					'letter-spacing' => '0',
					'subsets'        => '',
					'color'          => '#1f1f1f',
					'text-transform' => 'none',
				),
				'output'   => array(
					array(
						'element' => 'h2',
					),
				),
			),
            
            'cocoon_heading3_typo' => array(
				'type'     => 'typography',
				'label'    => esc_html__( 'Heading 3', 'cocoon' ),
				'section'  => 'headings_typo',
				'priority' => 10,
				'default'  => array(
					'font-family'    => 'Poppins',
					'variant'        => '500',
					'font-size'      => '30px',
					'line-height'    => '1.3',
					'letter-spacing' => '0',
					'subsets'        => '',
					'color'          => '#1f1f1f',
					'text-transform' => 'none',
				),
				'output'   => array(
					array(
						'element' => 'h3',
					),
				),
			),
            
            'cocoon_heading4_typo' => array(
				'type'     => 'typography',
				'label'    => esc_html__( 'Heading 4', 'cocoon' ),
				'section'  => 'headings_typo',
				'priority' => 10,
				'default'  => array(
					'font-family'    => 'Poppins',
					'variant'        => '500',
					'font-size'      => '24px',
					'line-height'    => '1.3',
					'letter-spacing' => '0',
					'subsets'        => '',
					'color'          => '#1f1f1f',
					'text-transform' => 'none',
				),
				'output'   => array(
					array(
						'element' => 'h4',
					),
				),
			),
            
            'cocoon_heading5_typo' => array(
				'type'     => 'typography',
				'label'    => esc_html__( 'Heading 5', 'cocoon' ),
				'section'  => 'headings_typo',
				'priority' => 10,
				'default'  => array(
					'font-family'    => 'Poppins',
					'variant'        => '500',
					'font-size'      => '20px',
					'line-height'    => '1.3',
					'letter-spacing' => '0',
					'subsets'        => '',
					'color'          => '#1f1f1f',
					'text-transform' => 'none',
				),
				'output'   => array(
					array(
						'element' => 'h5',
					),
				),
			),
            
            'cocoon_heading6_typo' => array(
				'type'     => 'typography',
				'label'    => esc_html__( 'Heading 6', 'cocoon' ),
				'section'  => 'headings_typo',
				'priority' => 10,
				'default'  => array(
					'font-family'    => 'Poppins',
					'variant'        => '500',
					'font-size'      => '18px',
					'line-height'    => '1.3',
					'letter-spacing' => '0',
					'subsets'        => '',
					'color'          => '#1f1f1f',
					'text-transform' => 'none',
				),
				'output'   => array(
					array(
						'element' => 'h6',
					),
				),
			),
            
            //  MENU - TYPOGRAPHY OPTIONS
            'cocoon_menu_typo' => array(
				'type'     => 'typography',
				'label'    => esc_html__( 'Menu', 'cocoon' ),
				'section'  => 'menu_typo',
				'priority' => 10,
				'default'  => array(
					'font-family'    => 'Poppins',
					'variant'        => '400',
					'font-size'      => '14px',
					'line-height'    => '37px',
					'letter-spacing' => '0',
					'subsets'        => '',
					'color'          => '#fff',
					'text-transform' => 'capitalize',
				),
				'output'   => array(
					array(
						'element' => 'header.header1 .navbar-nav .nav-link, header.header1 .extra-nav li a',
					),
				),
			),
            
            'cocoon_submenu_typo' => array(
				'type'     => 'typography',
				'label'    => esc_html__( 'Submenu Item', 'cocoon' ),
				'section'  => 'menu_typo',
				'priority' => 10,
				'default'  => array(
					'font-family'    => 'Poppins',
					'variant'        => '400',
					'font-size'      => '14px',
					'line-height'    => '1.4',
					'letter-spacing' => '0',
					'subsets'        => '',
					'color'          => '#1f1f1f',
					'text-transform' => 'capitalize',
				),
				'output'   => array(
					array(
						'element' => 'header.header1 #main-nav .navbar-nav .dropdown-menu > li > a, #main-nav .navbar-nav .mega-menu .dropdown-menu .mega-menu-inner .menu-item-mega a,
                        .header-btn-wrapper .extra-menu-item.ca-join-now ul.ca-user-menu li a',
					),
				),
			),
            
            // LOGO OPTIONS
            'logo'              => array(
				'type'        => 'image',
				'label'       => esc_html__( 'Logo', 'cocoon' ),
				'description' => esc_html__( 'This logo is used for all site.', 'cocoon' ),
				'section'     => 'logo',
				'default'     => '',
				'priority'    => 20,
			),
            
            'logo-white'              => array(
				'type'        => 'image',
				'label'       => esc_html__( 'Logo White', 'cocoon' ),
				'description' => esc_html__( 'This white version of the logo can be used for transparent header.', 'cocoon' ),
				'section'     => 'logo',
				'default'     => '',
				'priority'    => 20,
			),
            
            'logo_text'         => array(
				'type'     => 'text',
				'label'    => esc_html__( 'Text Logo', 'cocoon' ),
				'section'  => 'logo',
				'priority' => 20,
				array(
					'setting'  => 'logo',
					'operator' => '!=',
					'value'    => '',
				),
			),
            
			'logo_width'         => array(
				'type'     => 'text',
				'label'    => esc_html__( 'Logo Width(px)', 'cocoon' ),
				'section'  => 'logo',
				'priority' => 20,
                'default'     => '150',
				array(
					'setting'  => 'logo',
					'operator' => '!=',
					'value'    => '',
				),
			),
            
			'logo_height'        => array(
				'type'     => 'text',
				'label'    => esc_html__( 'Logo Height(px)', 'cocoon' ),
				'section'  => 'logo',
				'priority' => 20,
                'default'     => '',
				array(
					'setting'  => 'logo',
					'operator' => '!=',
					'value'    => '',
				),
			),
            
			'logo_margins'       => array(
				'type'     => 'spacing',
				'label'    => esc_html__( 'Logo Margin', 'cocoon' ),
                'description' => '',
				'section'  => 'logo',
				'priority' => 20,
				'default'  => array(
					'top'    => '0px',
					'bottom' => '0px',
					'left'   => '0px',
					'right'  => '0px',
				),
				array(
					'setting'  => 'logo',
					'operator' => '!=',
					'value'    => '',
				),
			),
            
            // MAIN HEADER OPTIONS
            'cocoon_header_style' => array(
                'type'        => 'select',
                'label'       => esc_html__( 'Layout Style', 'cocoon' ),
                'description' => esc_html__('Choose your header version.', 'cocoon'),
                'section'     => 'header',
                'default'     => 'header1',
                'priority'    => 10,
                'choices'     => array(
                    'header1' => esc_html__( 'Header 1 - Default', 'cocoon' ),
                ),
            ),

            'cocoon_sticky_header' => array(
                'type'        => 'switch',
                'label'       => esc_html__( 'Sticky Header', 'cocoon' ),
                'description' => esc_html__( 'Turn the switch "OFF" to disable Sticky Header.', 'cocoon' ),
                'section'     => 'header',
                'default'     => true,
                'priority'    => 10,
                'active_callback' => array(
					array(
						'setting'  => 'cocoon_header_style',
						'value'    => 'header1',
						'operator' => '==',
					),
				),
            ),
            
            'cocoon_fullwidth_header' => array(
                'type'        => 'switch',
                'label'       => esc_html__( 'Full Width Header', 'cocoon' ),
                'description' => esc_html__( 'Turn the switch "OFF" to disable Full Width Header.', 'cocoon' ),
                'section'     => 'header',
                'default'     => true,
                'priority'    => 10,
                'active_callback' => array(
					array(
						'setting'  => 'cocoon_header_style',
						'value'    => 'header1',
						'operator' => '==',
					),
				),
            ),
            
            'cocoon_menu_buttons' => array(
                'type'        => 'switch',
                'label'       => esc_html__( 'Menu Buttons', 'cocoon' ),
                'description' => esc_html__( 'Turn the switch "OFF" to disable all the buttons from the header.', 'cocoon' ),
                'section'     => 'header',
                'default'     => true,
                'priority'    => 10,
            ),
            
            'menu_extras'   => array(
				'type'            => 'multicheck',
				'label'           => esc_html__( 'Menu Extras', 'cocoon' ),
                'description'     => '',
				'section'         => 'header',
				'default'         => array( 'account', 'cart', 'search', 'wishlist' ),
				'priority'        => 10,
				'choices'         => array(
					'account'   => esc_html__( 'Account', 'cocoon' ),
					'cart'      => esc_html__( 'Cart', 'cocoon' ),
                    'search'    => esc_html__( 'Search', 'cocoon' ),
                    'wishlist'  => esc_html__( 'Wishlist', 'cocoon' ),
				),
                'active_callback' => array(
					array(
						'setting'  => 'cocoon_menu_buttons',
						'value'    => 1,
						'operator' => '==',
					),
				),
			),
            
            'cocoon_ajax_search' => array(
                'type'        => 'switch',
                'label'       => esc_html__( 'AJAX Search', 'cocoon' ),
                'description' => esc_html__( 'Turn the switch "OFF" to disable ajax search.', 'cocoon' ),
                'section'     => 'header',
                'default'     => true,
                'priority'    => 10,
                'active_callback' => array(
					array(
						'setting'  => 'cocoon_menu_buttons',
						'value'    => 1,
						'operator' => '==',
					),
                    array(
						'setting'  => 'menu_extras',
						'operator' => 'contains',
						'value'    => 'search',
					),
				),
            ),
            
            //  FOOTER OPTIONS
            'cocoon_footer_skin' => array(
                'type'        => 'select',
                'label'       => esc_html__( 'Footer Skin', 'cocoon' ),
                'description' => esc_html__('Choose your footer style-skin.', 'cocoon'),
                'section'     => 'footer',
                'default'     => 'dark-skin',
                'priority'    => 10,
                'choices'     => array(
                    'light-skin' => esc_html__( 'Light Skin', 'cocoon' ),
                    'dark-skin'  => esc_html__( 'Dark Skin', 'cocoon' ),
                ),
            ),            
            
            'cocoon_footer_info' => array(
                'type'          => 'switch',
                'label'         => esc_html__( 'Footer Info Section', 'cocoon' ),
                'description'   => esc_html__( 'Turn the switch "OFF" to remove the footer info section. ', 'cocoon' ),
                'section'       => 'footer',
                'default'       => true,
                'priority'      => 10,
            ),

            'cocoon_footer_sidebar_1' => array(
                'type'          => 'select',
                'label'         => esc_html__( 'Footer Sidebar Column 1', 'cocoon' ),
                'description'   => '',
                'section'       => 'footer',
                'default'       => 'col-lg-6 col-md-6 col-sm-6 col-12',
                'priority'      => 10,
                'choices'       => array(
                    'col-12' => esc_html__( 'Full Width', 'cocoon' ),
                    'col-lg-9 col-md-9 col-sm-9 col-12'  => esc_html__( '3/4', 'cocoon' ),
                    'col-lg-8 col-md-8 col-sm-8 col-12'  => esc_html__( '2/3', 'cocoon' ),
                    'col-lg-6 col-md-6 col-sm-6 col-12'  => esc_html__( '1/2', 'cocoon' ),
                    'col-lg-4 col-md-4 col-sm-4 col-12'  => esc_html__( '1/3', 'cocoon' ),
                    'col-lg-3 col-md-6 col-sm-12'  => esc_html__( '1/4', 'cocoon' ),
                    'disabled'     => esc_html__( 'disabled', 'cocoon' ),
                ),
                'active_callback' => array(
                    array(
                        'setting'  => 'cocoon_footer_info',
                        'value'    => true,
                        'operator' => '==',
                    ),
                ),
            ),

            'cocoon_footer_sidebar_2' => array(
                'type'          => 'select',
                'label'         => esc_html__( 'Footer Sidebar Column 2', 'cocoon' ),
                'description'   => '',
                'section'       => 'footer',
                'default'       => 'col-lg-3 col-md-6 col-sm-12',
                'priority'      => 10,
                'choices'       => array(
                    'col-12' => esc_html__( 'Full Width', 'cocoon' ),
                    'col-lg-9 col-md-9 col-sm-9 col-12'  => esc_html__( '3/4', 'cocoon' ),
                    'col-lg-8 col-md-8 col-sm-8 col-12'  => esc_html__( '2/3', 'cocoon' ),
                    'col-lg-6 col-md-6 col-sm-6 col-12'  => esc_html__( '1/2', 'cocoon' ),
                    'col-lg-4 col-md-4 col-sm-4 col-12'  => esc_html__( '1/3', 'cocoon' ),
                    'col-lg-3 col-md-6 col-sm-12'  => esc_html__( '1/4', 'cocoon' ),
                    'disabled'     => esc_html__( 'disabled', 'cocoon' ),
                ),
                'active_callback' => array(
                    array(
                        'setting'  => 'cocoon_footer_info',
                        'value'    => true,
                        'operator' => '==',
                    ),
                ),
            ),

            'cocoon_footer_sidebar_3' => array(
                'type'          => 'select',
                'label'         => esc_html__( 'Footer Sidebar Column 3', 'cocoon' ),
                'description'   => '',
                'section'       => 'footer',
                'default'       => 'col-lg-3 col-md-6 col-sm-12',
                'priority'      => 10,
                'choices'       => array(
                    'col-12' => esc_html__( 'Full Width', 'cocoon' ),
                    'col-lg-9 col-md-9 col-sm-9 col-12'  => esc_html__( '3/4', 'cocoon' ),
                    'col-lg-8 col-md-8 col-sm-8 col-12'  => esc_html__( '2/3', 'cocoon' ),
                    'col-lg-6 col-md-6 col-sm-6 col-12'  => esc_html__( '1/2', 'cocoon' ),
                    'col-lg-4 col-md-4 col-sm-4 col-12'  => esc_html__( '1/3', 'cocoon' ),
                    'col-lg-3 col-md-6 col-sm-12'  => esc_html__( '1/4', 'cocoon' ),
                    'disabled'     => esc_html__( 'disabled', 'cocoon' ),
                ),
                'active_callback' => array(
                    array(
                        'setting'  => 'cocoon_footer_info',
                        'value'    => true,
                        'operator' => '==',
                    ),
                ),
            ),

            'cocoon_footer_sidebar_4' => array(
                'type'          => 'select',
                'label'         => esc_html__( 'Footer Sidebar Column 4', 'cocoon' ),
                'description'   => '',
                'section'       => 'footer',
                'default'       => 'disabled',
                'priority'      => 10,
                'choices'       => array(
                    'col-12' => esc_html__( 'Full Width', 'cocoon' ),
                    'col-lg-9 col-md-9 col-sm-9 col-12'  => esc_html__( '3/4', 'cocoon' ),
                    'col-lg-8 col-md-8 col-sm-8 col-12'  => esc_html__( '2/3', 'cocoon' ),
                    'col-lg-6 col-md-6 col-sm-6 col-12'  => esc_html__( '1/2', 'cocoon' ),
                    'col-lg-4 col-md-4 col-sm-4 col-12'  => esc_html__( '1/3', 'cocoon' ),
                    'col-lg-3 col-md-6 col-sm-12'  => esc_html__( '1/4', 'cocoon' ),
                    'disabled'     => esc_html__( 'disabled', 'cocoon' ),
                ),
                'active_callback' => array(
                    array(
                        'setting'  => 'cocoon_footer_info',
                        'value'    => true,
                        'operator' => '==',
                    ),
                ),
            ),
            
            'cocoon_copyrights' => array(
                'type'          => 'textarea',
                'label'         => esc_html__( 'Copyrights text', 'cocoon' ),
                'description'   => esc_html__( 'Enter your Copyright Text (HTML allowed).', 'cocoon' ),
                'default'       => '&copy; Cocoon. Developed by <a href="http://themeforest.net/user/gnodesign/portfolio?ref=gnodesign" target="_blank">Gnodesign</a>',
                'section'       => 'footer',
                'priority'      => 10,
            ),
            
            'cocoon_footer_socials'    => array(
				'type'            => 'repeater',
				'label'           => esc_html__( 'Social Media', 'cocoon' ),
                'description'     => esc_html__( 'Choose the social media that you want to be displayed in the footer.', 'cocoon' ),
				'section'         => 'footer',
				'priority'        => 10,
				'default'         => '',
				'fields'          => array(
                    'social_type' => array(
                        'type'        => 'select',
                        'label'       => esc_html__( 'Social Media Type', 'cocoon' ),
						'description' => esc_html__( 'Choose your social media type.', 'cocoon' ),
						'default'     => '',
                        'priority'      => 10,
                        'choices'       => array(
                            '' => '-',
                            'facebook' => 'Facebook',
                            'twitter' => 'Twitter',
                            'google-plus' => 'Google Plus',
                            'instagram' => 'Instagram',
                            'linkedin' => 'LinkedIN',
                            'pinterest' => 'Pinterest',
                            'tumblr' => 'Tumblr',
                            'github' => 'GitHub',
                            'dribbble' => 'Dribbble',
                            'rss' => 'RSS',
                            'wordpress' => 'WordPress',
                            'amazon'=> 'Amazon',
                            'dropbox'=> 'Dropbox',
                            'paypal'=> 'PayPal',
                            'yahoo' => 'Yahoo',
                            'flickr' => 'Flickr',
                            'reddit' => 'Reddit',
                            'vimeo' => 'Vimeo',
                            'spotify' => 'Spotify',
                            'youtube' => 'YouTube',
                        ),
                    ),
					'link_url' => array(
						'type'        => 'text',
						'label'       => esc_html__( 'Social URL', 'cocoon' ),
						'description' => esc_html__( 'Enter the URL for this social', 'cocoon' ),
						'default'     => '',
					),
				)
			),
            
            //GENERAL BLOG OPTIONS
            'cocoon_blog_page_header_bg'   => array(
				'type'            => 'upload',
				'label'           => esc_html__( 'Page Header Background Image', 'cocoon' ),
                'description'     => esc_html__( 'Upload your a background image for the "page header"', 'cocoon' ),
				'section'         => 'blog',
				'priority'        => 10,
			),
            
            'cocoon_blog_title' => array(
                'type'        => 'text',
                'label'       => esc_html__( 'Blog Title', 'cocoon' ),
                'default'     => esc_html__( 'Latest News', 'cocoon' ),
                'section'     => 'blog',
                'priority'    => 10,
            ),
            
            'cocoon_blog_meta' => array(
                'type'        => 'multicheck',
                'label'       => esc_html__( 'Meta Informations on Blog Posts', 'cocoon' ),
                'description' => esc_html__( 'Set which elements of posts meta data you want to display on blog and archive pages.', 'cocoon' ),
                'section'     => 'blog',
                'default'     => array('author', 'date', 'cat', 'com'),
                'priority'    => 10,
                'choices'     => array(
                    'author' 	=> esc_html__( 'Author', 'cocoon' ),
                    'date' 		=> esc_html__( 'Date', 'cocoon' ),
                    'cat' 		=> esc_html__( 'Categories', 'cocoon' ),
                    'tags' 		=> esc_html__( 'Tags', 'cocoon' ),
                    'com' 		=> esc_html__( 'Comments', 'cocoon' ),
                ),
            ),
            
            'cocoon_blog_sidebar_custom'                => array(
				'type'     => 'custom',
				'section'  => 'blog',
				'default'  => '<hr>',
				'priority' => 10,
			),
            
            'cocoon_blog_sidebar'  => array(
                'type'          => 'switch',
				'label'         => esc_html__( 'Blog Sidebar', 'cocoon' ),
                'description'   => esc_html__('Turn the switch "OFF" if you want to disable the sidebar on the blog pages.', 'cocoon'),
				'section'       => 'blog',
				'priority'      => 10,
				'default'       => 1,
			),
            
            'cocoon_blog_sidebar_side' => array(
                'type'          => 'select',
                'label'         => esc_html__( 'Blog Sidebar Side', 'cocoon' ),
                'description'   => esc_html__('Choose on which side you want the sidebar to be.', 'cocoon'),
                'section'       => 'blog',
                'default'       => 'sidebar-right',
                'priority'      => 10,
                'choices'       => array(
                    'sidebar-right'  => esc_html__( 'Sidebar Right', 'cocoon' ),
                    'sidebar-left'   => esc_html__( 'Sidebar Left', 'cocoon' ),
                ),
                'active_callback' => array(
					array(
						'setting'  => 'cocoon_blog_sidebar',
						'operator' => '==',
						'value'    => 1,
					),
				),
            ),
            
            
            //SINGLE POST OPTIONS
            'cocoon_single_blog_meta' => array(
                'type'        => 'multicheck',
                'label'       => esc_html__( 'Meta Informations on Single Blog Posts', 'cocoon' ),
                'description' => esc_html__( 'Set which elements of posts meta data you want to display on single post.', 'cocoon' ),
                'section'     => 'single_blog_post',
                'default'     => array('author', 'date', 'cat', 'com'),
                'priority'    => 10,
                'choices'     => array(
                    'author' 	=> esc_html__( 'Author', 'cocoon' ),
                    'date' 		=> esc_html__( 'Date', 'cocoon' ),
                    'cat' 		=> esc_html__( 'Categories', 'cocoon' ),
                    'tags' 		=> esc_html__( 'Tags', 'cocoon' ),
                    'com' 		=> esc_html__( 'Comments', 'cocoon' ),
                ),
            ),
            
            'cocoon_post_share'        => array(
				'type'            => 'switch',
				'label'           => esc_html__( 'Show Sharing Icons', 'cocoon' ),
				'description'     => esc_html__( 'Display social sharing icons on single post', 'cocoon' ),
				'section'         => 'single_blog_post',
				'default'         => true,
				'priority'        => 10,
			),
            
            //SEARCH PAGE OPTIONS
            'cocoon_search_page_header_bg'   => array(
				'type'            => 'upload',
				'label'           => esc_html__( 'Page Header Background Image', 'cocoon' ),
                'description'     => esc_html__( 'Upload your a background image for the "page header"', 'cocoon' ),
				'section'         => 'search_page',
				'priority'        => 10,
			),
            
            //404 PAGE OPTIONS
            'cocoon_404_page_header_bg'   => array(
				'type'            => 'upload',
				'label'           => esc_html__( 'Page Header Background Image', 'cocoon' ),
                'description'     => esc_html__( 'Upload your a background image for the "page header"', 'cocoon' ),
				'section'         => 'page_not_found',
				'priority'        => 10,
			),
            
            // MAP OPTIONS
            'cocoon_gmap_api_key'                     => array(
				'type'          => 'text',
				'label'         => esc_html__( 'API Key', 'cocoon' ),
                'description'   => esc_html__('Add your Google Map API Key here', 'cocoon'),
				'section'       => 'map_options',
				'priority'      => 10,
				'default'       => '',
			),

            
            // WOOCOMMERCE            
            'cocoon_shop_page_header_bg'    => array(
				'type'            => 'upload',
				'label'           => esc_html__( 'Page Header Background Image', 'cocoon' ),
                'description'     => esc_html__( 'Upload your a background image for the "shop page"', 'cocoon' ),
				'section'         => 'shop_layout',
				'priority'        => 10,
			),
            
            'cocoon_shop_sidebar'  => array(
                'type'          => 'switch',
				'label'         => esc_html__( 'Shop Sidebar', 'cocoon' ),
                'description'   => esc_html__('Turn the switch "OFF" if you want to disable the sidebar on the shop page.', 'cocoon'),
				'section'       => 'shop_layout',
				'priority'      => 10,
				'default'       => 1,
			),
            
            'cocoon_shop_sidebar_side' => array(
                'type'          => 'select',
                'label'         => esc_html__( 'Shop Sidebar Side', 'cocoon' ),
                'description'   => esc_html__('Choose on which side you want the sidebar to be.', 'cocoon'),
                'section'       => 'shop_layout',
                'default'       => 'sidebar-right',
                'priority'      => 10,
                'choices'       => array(
                    'sidebar-right'  => esc_html__( 'Sidebar Right', 'cocoon' ),
                    'sidebar-left'   => esc_html__( 'Sidebar Left', 'cocoon' ),
                ),
                'active_callback' => array(
                    array(
                        'setting'  => 'cocoon_shop_sidebar',
                        'value'    => true,
                        'operator' => '==',
                    ),
                ),
            ),
            
            
            // Badges
            'cocoon_show_badges'            => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Show Badges', 'cocoon' ),
				'description' => esc_html__( 'Turn the switch "OFF" if you want to hide badges on every product.', 'cocoon' ),
				'section'     => 'shop_badges',
				'default'     => 1,
				'priority'    => 20,
			),
            
            'cocoon_badges'                         => array(
				'type'            => 'multicheck',
				'label'           => esc_html__( 'Badges', 'cocoon' ),
				'description'     => esc_html__( 'Select which badges you want to show', 'cocoon' ),
				'section'         => 'shop_badges',
				'default'         => array( 'hot', 'sale', 'outofstock' ),
				'priority'        => 20,
				'choices'         => array(
					'hot'        => esc_html__( 'Hot', 'cocoon' ),
					'sale'       => esc_html__( 'Sale', 'cocoon' ),
					'outofstock' => esc_html__( 'Out Of Stock', 'cocoon' ),
				),
				'active_callback' => array(
					array(
						'setting'  => 'cocoon_show_badges',
						'operator' => '==',
						'value'    => 1,
					),
				),
			),
            
			'cocoon_hot_text'                              => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Custom Hot Text', 'cocoon' ),
				'section'         => 'shop_badges',
				'default'         => 'Hot',
				'priority'        => 20,
				'active_callback' => array(
					array(
						'setting'  => 'cocoon_show_badges',
						'operator' => '==',
						'value'    => 1,
					),
					array(
						'setting'  => 'cocoon_badges',
						'operator' => 'contains',
						'value'    => 'hot',
					),
				),
			),
            
			'cocoon_outofstock_text'                       => array(
				'type'            => 'text',
				'label'           => esc_html__( 'Custom Out Of Stock Text', 'cocoon' ),
				'section'         => 'shop_badges',
				'default'         => 'Sold Out',
				'priority'        => 20,
				'active_callback' => array(
					array(
						'setting'  => 'cocoon_show_badges',
						'operator' => '==',
						'value'    => 1,
					),
					array(
						'setting'  => 'cocoon_badges',
						'operator' => 'contains',
						'value'    => 'outofstock',
					),
				),
			),
            
            
            // Product Grid
            'cocoon_secondary_thumb'               => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Secondary Product Thumbnail', 'cocoon' ),
				'description' => esc_html__( 'Turn the switch "OFF" to disable secondary product thumbnail when hover over the main product image.', 'cocoon' ),
				'section'     => 'product_grid',
				'default'     => 1,
				'priority'    => 20,
			),
            
			'cocoon_product_quick_view'             => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Quick View', 'cocoon' ),
				'description' => esc_html__( 'Turn the switch "OFF" to disable the quick view in every product.', 'cocoon' ),
				'section'     => 'product_grid',
				'default'     => 1,
				'priority'    => 20,
			),
            
            'cocoon_product_category'               => array(
				'type'        => 'toggle',
				'label'       => esc_html__( 'Categories', 'cocoon' ),
				'description' => esc_html__( 'Turn the switch "OFF" to hide the categories in every product.', 'cocoon' ),
				'section'     => 'product_grid',
				'default'     => 1,
				'priority'    => 20,
			),
            
            'shop_expert_length'                    => array(
				'type'            => 'number',
				'label'           => esc_html__( 'Product Description Expert Length', 'cocoon' ),
				'section'         => 'product_grid',
				'default'         => 15,
				'priority'        => 20,
			),
            
            
            // Single Product
            'cocoon_product_nav'                    => array(
				'type'        => 'switch',
				'label'       => esc_html__( 'Product Navigation', 'cocoon' ),
				'description' => esc_html__( 'Turn the switch "OFF" to disable product navigation', 'cocoon' ),
				'section'     => 'single_product',
				'default'     => 1,
				'priority'    => 20,
			),
            
            'product_zoom'                          => array(
				'type'        => 'switch',
				'label'       => esc_html__( 'Product Zoom', 'cocoon' ),
				'description' => esc_html__( 'Check this option to show a bigger size product image on mouseover.', 'cocoon' ),
				'section'     => 'single_product',
				'default'     => 1,
				'priority'    => 20,
			),
            
            'product_share'                         => array(
				'type'        => 'switch',
				'label'       => esc_html__( 'Show Sharing Buttons', 'cocoon' ),
				'description' => esc_html__( 'Turn the switch "OFF" if you want to hide the sharing buttons.', 'cocoon' ),
				'section'     => 'single_product',
				'default'     => 1,
				'priority'    => 20,
			),
            
            'product_upsells_custom'                => array(
				'type'     => 'custom',
				'section'  => 'single_product',
				'default'  => '<hr>',
				'priority' => 20,
			),
            
            'product_upsells'                       => array(
				'type'        => 'switch',
				'label'       => esc_html__( 'Show Up-sells Products', 'cocoon' ),
				'description' => esc_html__( 'Turn the switch "OFF" if you want to hide Up-sells products.', 'cocoon' ),
				'section'     => 'single_product',
				'default'     => 1,
				'priority'    => 20,
			),
            
            'product_related_custom'                => array(
				'type'     => 'custom',
				'section'  => 'single_product',
				'default'  => '<hr>',
				'priority' => 20,
			),
            
            'product_related'                       => array(
				'type'        => 'switch',
				'label'       => esc_html__( 'Show Related Products', 'cocoon' ),
				'description' => esc_html__( 'Turn the switch "OFF" if you want to hide related products.', 'cocoon' ),
				'section'     => 'single_product',
				'default'     => 1,
				'priority'    => 20,
			),
            
            'custom_product_tab_hr'                => array(
				'type'     => 'custom',
				'section'  => 'single_product',
				'default'  => '<hr>',
				'priority' => 20,
			),
            
            'custom_product_tab_1'                  => array(
				'type'        => 'text',
				'label'       => esc_html__( 'Custom Product Tab 1', 'cocoon' ),
				'description' => esc_html__( 'Enter the text for custom product tab 1', 'cocoon' ),
				'section'     => 'single_product',
				'default'     => esc_html__( 'Size Guide', 'cocoon' ),
				'priority'    => 20,
			),
            
			'custom_product_tab_2'                  => array(
				'type'        => 'text',
				'label'       => esc_html__( 'Custom Product Tab 2', 'cocoon' ),
				'description' => esc_html__( 'Enter the text for custom product tab 2', 'cocoon' ),
				'section'     => 'single_product',
				'default'     => esc_html__( 'Shipping', 'cocoon' ),
				'priority'    => 20,
			),
            
            
        ), // end of fields array
    ); // end of return array       
} // end of function cocoon_customize_settings


function cocoon_customize_init() {
	$cocoon_customize = new Cocoon_Customize( cocoon_customize_settings() );
}


if ( class_exists( 'Kirki' ) ) {
	add_action( 'init', 'cocoon_customize_init', 30 );
} else {
	$cocoon_customize = new Cocoon_Customize( cocoon_customize_settings() );
}
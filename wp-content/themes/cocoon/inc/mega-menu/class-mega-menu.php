<?php
class Cocoon_Mega_Menu {
	/**
	 * Cocoon_Mega_Menu constructor.
	 */
	public function __construct() {

		$this->load();
		$this->init();

		add_filter( 'wp_edit_nav_menu_walker', array( $this, 'edit_nav_menu_walker' ) );
	}

	/**
	 * Load files
	 */
	private function load() {
		include get_template_directory() . '/inc/mega-menu/class-menu-edit.php';
	}

	/**
	 * Initialize
	 */
	private function init() {
		if ( is_admin() ) {
			new Cocoon_Mega_Menu_Edit();
		}
	}

	/**
	 * Change the default nav menu walker
	 *
	 * @return string
	 */
	public function edit_nav_menu_walker() {
		return 'Cocoon_Mega_Menu_Walker_Edit';
	}
}

add_action( 'init', function() {
	global $mega_menu;

	$mega_menu = new Cocoon_Mega_Menu();
} );
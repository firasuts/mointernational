<% if ( depth == 0 ) { %>
<a href="#" class="media-menu-item active" data-title="<?php esc_attr_e( 'Mega Menu Content', 'cocoon' ) ?>" data-panel="mega"><?php esc_html_e( 'Mega Menu', 'cocoon' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Mega Menu Background', 'cocoon' ) ?>" data-panel="background"><?php esc_html_e( 'Background', 'cocoon' ) ?></a>
<div class="separator"></div>
<% } else if ( depth == 1 ) { %>
<a href="#" class="media-menu-item active" data-title="<?php esc_attr_e( 'Menu Content', 'cocoon' ) ?>" data-panel="content"><?php esc_html_e( 'Menu Content', 'cocoon' ) ?></a>
<a href="#" class="media-menu-item" data-title="<?php esc_attr_e( 'Menu General', 'cocoon' ) ?>" data-panel="general"><?php esc_html_e( 'General', 'cocoon' ) ?></a>
<% } else { %>
<a href="#" class="media-menu-item active" data-title="<?php esc_attr_e( 'Menu General', 'cocoon' ) ?>" data-panel="general"><?php esc_html_e( 'General', 'cocoon' ) ?></a>
<% } %>

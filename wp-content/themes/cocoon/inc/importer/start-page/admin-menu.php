<?php 
/**
*
* @package Cocoon
*
* @since 1.0.0
* 
* ========================
* COCOON ADMIN MENU FILE
* ========================
*     
**/



class Cocoon_AdminMenu {
    
	public static $settings;

	public function __construct($core) {
		$this->core = $core;
		$settings = array();
		$this::$settings = wp_parse_args( $settings, array(
			'page-title' => esc_html__( 'Cocoon', 'cocoon' ),
			'menu-title' => esc_html__( 'Cocoon', 'cocoon' ),
			'capability' => 'edit_theme_options',
			'menu-slug' => 'cocoon_theme',
			'function' => array(&$this, 'menu_content'),
			'icon_url' => get_template_directory_uri() . '/inc/importer/admin-tpl/assets/img/cocoon.png'
        ) );
	}
    
	public function run(){
		add_action( 'admin_menu', array($this, 'menu') );
	}
    
    
	public function get_settings($setting) {
		return $this::$settings[$setting];
	}
    
	public function menu(){
		$page = add_menu_page( $this::$settings['page-title'], $this::$settings['menu-title'], $this::$settings['capability'], $this::$settings['menu-slug'], $this::$settings['function'], $this::$settings['icon_url'], 2 ); 
        
		add_action('load-' . $page, array($this,'menu_scripts'));
	}
    
    
	public function menu_content() {
		require_once get_template_directory() . '/inc/importer/admin-tpl/start-page.php';
	}

    
	public function menu_scripts() {
		add_action( 'admin_enqueue_scripts', array($this,'register_scripts'), 10, 1 );
	}

    
	public function register_scripts($hook) {
		wp_enqueue_script( 'clipboard', get_template_directory_uri() . '/inc/importer/admin-tpl/assets/js/clipboard.min.js', array( 'jquery' ), false, false );

		wp_enqueue_script( 'image-picker', get_template_directory_uri() . '/inc/importer/admin-tpl/assets/js/image-picker.min.js', array( 'jquery' ), false, false );

		wp_enqueue_script( 'notify', get_template_directory_uri() . '/inc/importer/admin-tpl/assets/js/notify.js', array( 'jquery' ), false, false );

		wp_enqueue_script( 'cocoon-start-scripts', get_template_directory_uri() . '/inc/importer/admin-tpl/assets/js/start-page.js', array( 'jquery' ), false, false );
		
		wp_enqueue_style( 'cocoon-admin-tabs', get_template_directory_uri() . '/inc/importer/admin-tpl/assets/css/start-page.css', false );

		wp_enqueue_style( 'image-picker-css', get_template_directory_uri() . '/inc/importer/admin-tpl/assets/css/image-picker.css' );

		wp_enqueue_style( 'notify-metro-css', get_template_directory_uri() . '/inc/importer/admin-tpl/assets/css/notify-metro.css' );
	}
}
<?php
/**
*
* @package Cocoon
*
* @since 1.0.0
*
* ========================
* EXTRA FILE
* ========================
*
**/




if( !function_exists('cocoon_check_import_requires') ) {
	function cocoon_check_import_requires() {
		$max_execution_time_cur = @ini_get( 'max_execution_time' );
		$max_execution_time_sug = 180;
		$memory_limit_cur = @ini_get( 'memory_limit' ); //WP_MAX_MEMORY_LIMIT;
		$memory_limit_sug = 128;
	?>


	<div class="table-php-requirements-container">
		<table class="table-php-requirements">

			<thead>
				<tr>
					<td colspan="4">
						<p><?php esc_html_e( 'In order to successfully import a demo, please ensure your server meets the following PHP configurations. Your hosting provider will help you modify server configurations, if required.', 'cocoon'); ?></p>
					</td>
				</tr>
				<tr>
					<th><?php esc_html_e( 'Directive', 'cocoon'); ?></th>
					<th><?php esc_html_e( 'Priority', 'cocoon'); ?></th>
					<th><?php esc_html_e( 'Least Suggested Value', 'cocoon'); ?></th>
					<th><?php esc_html_e( 'Current Value', 'cocoon'); ?></th>
				</tr>
				<tr class="spacer"></tr>
			</thead>


			<tbody>
				<tr>
					<td><?php echo esc_html( 'DOMDocument'); ?></td>
					<td><?php echo esc_html( 'High'); ?></td>
					<td class="bold"><?php esc_html_e( 'Supported', 'cocoon'); ?></td>
					<td class="bold <?php echo class_exists( 'DOMDocument' ) ? esc_attr('ok') : esc_attr('notok'); ?>"><?php echo class_exists( 'DOMDocument' ) ? 'Supported' : 'Not Supported'; ?></td>
				</tr>
				<tr>
					<td><?php echo esc_html( 'memory_limit'); ?></td>
					<td><?php echo esc_html( 'High'); ?></td>
					<td class="bold"><?php echo esc_html($memory_limit_sug); ?>M</td>
					<td class="bold <?php echo intval($memory_limit_cur) >= $memory_limit_sug ? esc_attr('ok') : esc_attr('notok'); ?>"><?php echo esc_html($memory_limit_cur); ?></td>
				</tr>
				<tr>
					<td><?php echo esc_html( 'max_execution_time*'); ?></td>
					<td><?php esc_html_e('Medium', 'cocoon'); ?></td>
					<td class="bold"><?php echo esc_html($max_execution_time_sug); ?></td>
                    
                    <?php if ( $max_execution_time_cur >= $max_execution_time_sug ) {
                        $max_execution_time = 'ok';
                    } else {
                        $max_execution_time = 'notok';
                    } ?>
                    
					<td class="bold <?php echo esc_attr($max_execution_time); ?>"><?php echo esc_html($max_execution_time_cur); ?></td>
				</tr>
			</tbody>

			<?php if( intval($memory_limit_cur) < $memory_limit_sug || $max_execution_time_cur < $max_execution_time_sug ) { ?>
                <tfoot>
                    <tr class="spacer"></tr>

                    <tr>
                        <td colspan="4" class="small">
                            <?php printf( esc_html__( 'To change PHP directives you need to modify %s file, more information about this you can %s or contact your hosting provider.', 'cocoon'), '<strong>php.ini</strong>' ,'<a href="http://goo.gl/I9f74U" target="_blank">search here</a>' ); ?>
                            <br>
                            <small><em><?php esc_html_e( '* Even if your current value of "max execution time" is lower than recommended, demo content can still be imported in most cases.', 'cocoon'); ?></em></small>
                        </td>
                    </tr>
                </tfoot>
			<?php } ?>

		</table>
	</div>
	<?php
	}
}

add_action( 'cocoon_import_tpl', 'cocoon_check_import_requires', 10, 1 );


/*** Required Plugin notice ***/
function cocoon_required_plugin_notice() {
    if ( ! function_exists('cocoon_stat_display') ) {
		$output = '<div class="notic notic-warning "><p>' . esc_html( 'Please install & activate the "Cocoon Core" plugin manually in order to proceed. You can do so by clicking "Begin installing plugin" on top of the screen.', 'cocoon') . '</p></div>';
        
        return $output;
	}
}

function cocoon_required_plugin_notice_output() {
    echo cocoon_required_plugin_notice();
}

add_action( 'cocoon_plugin_tpl', 'cocoon_required_plugin_notice_output' );
add_action( 'cocoon_import_tpl', 'cocoon_required_plugin_notice_output' );
add_action( 'cocoon_systatus_tpl', 'cocoon_required_plugin_notice_output' );

<?php
/**
*
* @package Cocoon
*
* @since 1.0.0
* 
* ========================
* WELCOME TEMPLATE FILE
* ========================
*     
**/





global $Cocoon_Core;
?>



<header class="cocoon-start-header">
    <div class="header-wrapper clearfix">
        <div class="c-3">
            <?php $themeinfo = wp_get_theme(); ?>
            <div class="theme_preview">
                <img height="180" src="<?php echo esc_url( $themeinfo->get_screenshot() ); ?>">
                <span class="ves">
                    <?php 
                        echo esc_html( $themeinfo->get('Version') );
                    ?>
                </span>
            </div>
        </div>
        <div class="c-9">
            <h1><?php esc_html_e( 'Welcome to Cocoon', 'cocoon' ); ?></h1>
            <p><?php esc_html_e( 'Thank you for purchasing and using Cocoon. To Get started, please install all the required and recommended plugins and import the Demo. For more information on how to use the theme, please check the themes documentation.', 'cocoon'); ?></p>
            
            <a href="https://themeforest.net/downloads" target="_blank" class="button button-primary rate-btn"><?php esc_html_e( 'Rate Cocoon', 'cocoon'); ?></a>
            
            <a href="https://themeforest.net/item/cocoon-modern-woocommerce-wordpress-theme/22220527/support?ref=gnodesign" target="_blank" class="button button-primary support-btn"><?php esc_html_e( 'Support', 'cocoon'); ?></a>
            <a href="https://youtu.be/iArIyTHBzvw" target="_blank" class="button button-primary support-btn"><?php esc_html_e( 'Installation Video', 'cocoon'); ?></a>
            <span id="cocoon-home-url" style="display:none;">
                <?php echo esc_url( home_url() ); ?>
            </span>
        </div>
    </div>
</header>





<section class="cocoon-start-content">

    <h2 class="nav-tab-wrapper">
        <a href="#" data-tab="cocoon-welcome" class="nav-tab nav-tab-active"><?php esc_html_e( 'Getting Started', 'cocoon' ); ?></a>
		<a href="#" data-tab="cocoon-plugins" class="nav-tab"><?php esc_html_e( 'Install Plugins', 'cocoon' ); ?></a>
		<a href="#" data-tab="cocoon-import" class="nav-tab"><?php esc_html_e( 'Import', 'cocoon' ); ?></a>
		<a href="#" data-tab="cocoon-system-status" class="nav-tab"><?php esc_html_e( 'System Status', 'cocoon' ); ?></a>
        
		<?php do_action( 'cocoon_start_tabs' ); ?>
	</h2>
    
    
    
    
	<div class="notifyjs"></div>
	
    
    
    
    <!-- Cocoon "Getting Started" nav content -->
    <div class="nav-content current" id="cocoon-welcome">
        <h2><?php esc_html_e( 'Thank you for choosing Cocoon!', 'cocoon'); ?></h2>
        <p><?php esc_html_e( 'Get your E-Commerce website up and running in no time just by following these simple steps:', 'cocoon' ); ?>
            <ol>
                <li><?php printf( esc_html__( 'Please make sure to install the %s. This way you won\'t loose any customizations that you may apply, in the future theme updates.', 'cocoon'), '<strong>Child-Theme</strong>' ); ?></li>
                <li><?php esc_html_e( 'Install & Activate Recommended and Required Plugins ', 'cocoon' ); ?><a href="#" data-tab="cocoon-plugins"><?php esc_html_e( 'Install Plugins', 'cocoon'); ?></a></li>
                <li><?php esc_html_e( 'Import the demo content by going to ', 'cocoon' ) ?><a data-tab="cocoon-import" href="#"><?php esc_html_e( 'Import', 'cocoon'); ?></a></li>
                <li><?php esc_html_e( 'That\'s it! Your website is ready!', 'cocoon' ); ?></li>
            </ol>
        </p>
        
        <p><?php printf( esc_html__( 'If you like our theme please support us by %s our theme. If you spot any bugs or have any features that you would like to see in Cocoon please don\'t hesitate to contact us.', 'cocoon'), '<a href="https://themeforest.net/downloads" target="_blank">rating</a>'); ?></p>
    
        
        
        <h3 style="margin-top: 40px;"><?php esc_attr_e( 'Buy license', 'cocoon' ); ?></h3>	
							
        <p><?php esc_attr_e( 'If you need another license for a new website you can buy it by clicking the button below:', 'cocoon'); ?></p>


        <a class="button button-primary envato-btn" target="_blank" href="https://themeforest.net/item/cocoon-modern-woocommerce-wordpress-theme/22220527?ref=gnodesign"><?php esc_attr_e( 'Purchase new license', 'cocoon'); ?></a>


        <p class="important-box">            
            <strong><?php esc_html_e( 'Important!', 'cocoon'); ?></strong> <a target="_blank" href="https://themeforest.net/licenses/standard"><?php esc_html_e( 'One standard license ', 'cocoon'); ?></a><?php printf( esc_html__( 'is valid only for %s. Running multiple websites on a single license is a copyright violation. %s1
            When moving a site from one domain to another please deregister the theme first.', 'cocoon'), '<strong>1 website</strong>', '<br/>' ); ?>
        </p>	
    
    
        <?php do_action( 'cocoon_welcome_content' ); ?>
	</div>
    

    <!-- Cocoon "Install Plugins" nav content -->
	<div class="nav-content" id="cocoon-plugins">
        <?php do_action( 'cocoon_plugin_tpl' ); ?>
	</div>

    

    <!-- Cocoon "Import" nav content -->
	<div class="nav-content" id="cocoon-import">
		<?php do_action( 'cocoon_import_tpl' ); ?>
	</div>


    <!-- Cocoon "System Status" nav content -->
	<div class="nav-content" id="cocoon-system-status">
		<?php do_action( 'cocoon_systatus_tpl' ); ?>
	</div>


	<?php do_action( 'cocoon_tabs_content' ); ?>
</section>

<div class="loader">
    <span class="circle"></span>
</div>
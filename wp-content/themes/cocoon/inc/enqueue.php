<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* ENQUEUE FUNCTIONS
* ========================
*     
**/





/* 
=====================================================
ENQUEUE STYLES
=====================================================
*/

function cocoon_styles() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style( 'mmenu', get_template_directory_uri() . '/assets/css/jquery.mmenu.css');
    wp_enqueue_style( 'owl-slider', get_template_directory_uri() . '/assets/css/owl.carousel.min.css');
    wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css');
    
    /* Icons */
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css');
    wp_enqueue_style( 'simple-line-icons', get_template_directory_uri() . '/assets/css/simple-line-icons.min.css');
    wp_enqueue_style( 'iconsmind', get_template_directory_uri() . '/assets/css/iconsmind.min.css');
    
    
    /* Main Styles */
    wp_enqueue_style( 'cocoon-default', get_template_directory_uri() . '/assets/css/default.css');
    wp_enqueue_style( 'cocoon-responsive', get_template_directory_uri() . '/assets/css/responsive.css');
    wp_enqueue_style( 'cocoon-main', get_template_directory_uri() . '/style.css');
}

add_action( 'wp_enqueue_scripts', 'cocoon_styles' );





/* 
=====================================================
ENQUEUE SCRIPTS
=====================================================
*/

function cocoon_scripts() {
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'mmenu', get_template_directory_uri() . '/assets/js/jquery.mmenu.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'owl-slider', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'imagesloaded' );
    wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'isotope', get_template_directory_uri() . '/assets/js/isotope.min.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'zoom', get_template_directory_uri() . '/assets/js/jquery.zoom.min.js', array( 'jquery' ), false, true );
    
    //Google Maps
    $google_api = cocoon_get_option('cocoon_gmap_api_key');
    if( !empty($google_api) ) {
        wp_enqueue_script( 'google-maps', 'https://maps.google.com/maps/api/js?key=' . esc_attr( $google_api ) . '&libraries=places' );        
    } // !empty($google_api)
    
    // Comment Reply Script
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    
    // Slick Slider
    if ( is_singular('product') ) {
        wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/js/slick.min.js', array( 'jquery' ), false, true );
    }
    
    // Photoswipe
    if ( is_singular() ) {
        
		$photoswipe = 'photoswipe';
		if ( wp_style_is( $photoswipe, 'registered' ) && ! wp_style_is( $photoswipe, 'enqueued' ) ) {
			wp_enqueue_style( $photoswipe );
		}

		$photoswipe_skin = 'photoswipe-default-skin';
		if ( wp_style_is( $photoswipe_skin, 'registered' ) && ! wp_style_is( $photoswipe_skin, 'enqueued' ) ) {
			wp_enqueue_style( $photoswipe_skin );
		}

		$photoswipe_ui = 'photoswipe-ui-default';
		if ( wp_script_is( $photoswipe_ui, 'registered' ) && ! wp_script_is( $photoswipe_ui, 'enqueued' ) ) {
			wp_enqueue_script( $photoswipe_ui );
		}
	}
    
    // Main Theme JS File
    wp_enqueue_script( 'cocoon-main', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), false, true );
    
    $ajax_url       = admin_url( 'admin-ajax.php', 'relative' );
    $ajax_search    = intval( cocoon_get_option( 'cocoon_ajax_search' ) );
    $product_zoom   = intval( cocoon_get_option( 'product_zoom' ) );
    $wc_active      = intval( cocoon_wc_is_activated() );
    
    $translations = array(
        'ajax_url'          => esc_url( $ajax_url ),
        'nonce'             => wp_create_nonce( '_cocoon_nonce' ),
        'ajax_search'       => $ajax_search,
        'cart_message'      => esc_html__( ' has been added to your cart.', 'cocoon' ),
        'product_zoom'      => $product_zoom,
        'wc_active'         => $wc_active,
    );
    
	wp_localize_script( 'cocoon-main', 'cocoon_settings', $translations );
}

add_action( 'wp_enqueue_scripts', 'cocoon_scripts' );





/* 
=====================================================
ADMIN ENQUEUE SCRIPTS
=====================================================
*/

function cocoon_admin_scripts( $hook ) {
    
}

add_action( 'admin_enqueue_scripts', 'cocoon_admin_scripts' );
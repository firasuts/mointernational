<?php

/**
*
* @package Cocoon
*
* @since 1.1.0
*
* ========================
* PRODUCT CATEGORY HEADER IMAGE
* ========================
*
**/
 




/**
 *  Category Header fields.
 *
 * @since  1.1.0
 */

function cocoon_wc_add_category_header_img() {

	global $woocommerce;
	?>

	<div class="form-field">
		<label><?php esc_html_e( 'Page Header Image', 'cocoon' ); ?></label>
		
        <div id="product_cat_header" style="float:left;margin-right:10px;">
            <img src="<?php echo wc_placeholder_img_src(); ?>" width="60px" height="60px" />
        </div>
		
        <div style="line-height:60px;">
			<input type="hidden" id="product_cat_header_id" name="product_cat_header_id" />
			<button type="submit" class="upload_header_button button"><?php esc_html_e( 'Upload/Add image', 'cocoon' ); ?></button>
			<button type="submit" class="remove_header_image_button button"><?php esc_html_e( 'Remove image', 'cocoon' ); ?></button>
		</div>

        
		<script type="text/javascript">
			// Only show the "remove image" button when needed
			if ( ! jQuery('#product_cat_header_id').val() ) {
                jQuery('.remove_header_image_button').hide();
            }
				
			// Uploading files
			var header_file_frame;
			
			jQuery(document).on( 'click', '.upload_header_button', function( event ) {
				event.preventDefault();

				// If the media frame already exists, reopen it.
				if ( header_file_frame ) {
					header_file_frame.open();
					return;
				}

				// Create the media frame.
				header_file_frame = wp.media.frames.downloadable_file = wp.media({
					title: '<?php _e( 'Choose an image', 'shopkeeper' ); ?>',
					button: {
						text: '<?php _e( 'Use image', 'shopkeeper' ); ?>',
					},
					multiple: false
				});

				// When an image is selected, run a callback.
				header_file_frame.on( 'select', function() {
					attachment = header_file_frame.state().get('selection').first().toJSON();
					jQuery('#product_cat_header_id').val( attachment.id );
					jQuery('#product_cat_header img').attr('src', attachment.url );
					jQuery('.remove_header_image_button').show();
				});

				// Finally, open the modal.
				header_file_frame.open();

			});

			jQuery(document).on( 'click', '.remove_header_image_button', function( event ){
				jQuery('#product_cat_header img').attr('src', '<?php echo wc_placeholder_img_src(); ?>');
				jQuery('#product_cat_header_id').val('');
				jQuery('.remove_header_image_button').hide();
				return false;
			});
		</script>

		<div class="clear"></div>
	</div>
	<?php
}

add_action( 'product_cat_add_form_fields', 'cocoon_wc_add_category_header_img' );





/**
 *  Edit category header field.
 *
 * @since  1.1.0
 */

function cocoon_wc_edit_category_header_img( $term, $taxonomy ) {

	global $woocommerce;

	$display_type      = get_woocommerce_term_meta( $term->term_id, 'display_type', true );
	$image 	           = '';
	$header_id 	       = absint( get_woocommerce_term_meta( $term->term_id, 'header_id', true ) );
	if ($header_id) {
		$image         = wp_get_attachment_url( $header_id );
    } else {
		$image         = wc_placeholder_img_src();
    } ?>


	<tr class="form-field">
		<th scope="row" valign="top"><label><?php esc_html_e( 'Page Header Image', 'cocoon' ); ?></label></th>
        
		<td>
			<div id="product_cat_header" style="float:left;margin-right:10px;">
                <img src="<?php echo $image; ?>" width="60px" height="60px" />
            </div>
			
            <div style="line-height:60px;">
				<input type="hidden" id="product_cat_header_id" name="product_cat_header_id" value="<?php echo $header_id; ?>" />
				<button type="submit" class="upload_header_button button"><?php esc_html_e( 'Upload/Add image', 'cocoon' ); ?></button>
				<button type="submit" class="remove_header_image_button button"><?php esc_html_e( 'Remove image', 'cocoon' ); ?></button>
			</div>

			<script type="text/javascript">			 

                if (jQuery('#product_cat_thumbnail_id').val() == 0) {
                    jQuery('.remove_image_button').hide();
                }
				 

                if (jQuery('#product_cat_header_id').val() == 0)
                     jQuery('.remove_header_image_button').hide();

				// Uploading files
				var header_file_frame;

				jQuery(document).on( 'click', '.upload_header_button', function( event ){
					event.preventDefault();

					// If the media frame already exists, reopen it.
					if ( header_file_frame ) {
						header_file_frame.open();
						return;
					}

					// Create the media frame.
					header_file_frame = wp.media.frames.downloadable_file = wp.media({
						title: '<?php esc_attr_e( 'Choose an image', 'cocoon' ); ?>',
						button: {
							text: '<?php esc_attr_e( 'Use image', 'cocoon' ); ?>',
						},
						multiple: false
					});

					// When an image is selected, run a callback.
					header_file_frame.on( 'select', function() {
						attachment = header_file_frame.state().get('selection').first().toJSON();
						jQuery('#product_cat_header_id').val( attachment.id );
						jQuery('#product_cat_header img').attr('src', attachment.url );
						jQuery('.remove_header_image_button').show();
					});

					// Finally, open the modal.
					header_file_frame.open();
				});

				jQuery(document).on( 'click', '.remove_header_image_button', function( event ){
					jQuery('#product_cat_header img').attr('src', '<?php echo wc_placeholder_img_src(); ?>');
					jQuery('#product_cat_header_id').val('');
					jQuery('.remove_header_image_button').hide();
					return false;
				});
			</script>

			<div class="clear"></div>
		</td>
	</tr>

	<?php
}

add_action( 'product_cat_edit_form_fields', 'cocoon_wc_edit_category_header_img', 10,2 );




/**
 *  Save function
 *
 * @since  1.1.0
 */

function cocoon_wc_category_header_img_save( $term_id, $tt_id, $taxonomy ) {	

	if ( isset( $_POST['product_cat_header_id'] ) ) {
        update_woocommerce_term_meta( $term_id, 'header_id', absint( $_POST['product_cat_header_id'] ) );
    }
		
	delete_transient( 'wc_term_counts' );

}

add_action( 'created_term', 'cocoon_wc_category_header_img_save', 10,3 );
add_action( 'edit_term', 'cocoon_wc_category_header_img_save', 10,3 );





/**
 *  Header column added to category admin.
 *
 * @since  1.1.0
 */

function cocoon_wc_product_cat_header_columns( $columns ) {

	$new_columns = array();
	$new_columns['cb'] = $columns['cb'];
	$new_columns['header'] = esc_html__( 'Page Header Image', 'cocoon' );
	unset( $columns['cb'] );

	return array_merge( $new_columns, $columns );

}

add_filter( 'manage_edit-product_cat_columns', 'cocoon_wc_product_cat_header_columns' );





/**
 *  Thumbnail column value added to category admin.
 *
 * @since  1.1.0
 */

function cocoon_wc_product_cat_header_column( $columns, $column, $id ) {

	global $woocommerce;

	if ( $column == 'header' ) {
		$image 			= '';
		$thumbnail_id 	= get_woocommerce_term_meta( $id, 'header_id', true );

		if ($thumbnail_id) {
            $image = wp_get_attachment_url( $thumbnail_id );
        } else {
            $image = wc_placeholder_img_src();
        }

		$columns .= '<img src="' . $image . '" alt="Thumbnail" class="wp-post-image" height="40" width="40" />';
	}

	return $columns;
	
}

add_filter( 'manage_product_cat_custom_column', 'cocoon_wc_product_cat_header_column', 10, 3 );





/**
 *  Get Header Image
 *
 * @since  1.1.0
 */

function cocoon_wc_get_cat_header_image_url($cat_ID = false) {

	if ($cat_ID==false && is_product_category()) {
		global $wp_query;
		
		// get the query object
		$cat = $wp_query->get_queried_object();
		
		// get the thumbnail id user the term_id
		$cat_ID = $cat->term_id;
	}

    $thumbnail_id = get_woocommerce_term_meta($cat_ID, 'header_id', true ); 

    // get the image URL
   return wp_get_attachment_url( $thumbnail_id ); 

}




function cocoon_show_category_header() {
	$category_header_src = cocoon_wc_get_cat_header_image_url();	
    
    if ( $category_header_src != '' ) {
        echo '<div class="woocommerce_category_header_image"><img src="' . $category_header_src . '" style="width:100%; height:auto; margin:0 0 20px 0" /></div>';
    } else {
        echo '';
    }
    
}

add_action('woocommerce_archive_description','show_category_header');
<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* PLUGIN ACTIVATION FILE
* ========================
*     
**/


/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/inc/plugins/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'cocoon_register_required_plugins' );




/**
 * Register the required plugins for this theme.
 */
function cocoon_register_required_plugins() {

	$plugins = array(
        
        array(
			'name'               => esc_html__( 'Cocoon Core', 'cocoon'),
			'slug'               => 'cocoon-plugin',
			'source'             => get_template_directory() . '/inc/plugins/cocoon-plugin.zip',
			'required'           => true,
			'version'            => '1.0.0',
			'force_activation'   => false,
		),
        array(
			'name'               => esc_html__( 'Visual Composer', 'cocoon'),
			'slug'               => 'js_composer',
			'source'             => get_template_directory() . '/inc/plugins/js_composer.zip',
			'required'           => true,
			'version'            => '5.5.2',
			'force_activation'   => false,
		),
		array(
			'name'               => esc_html__( 'Revolution Slider', 'cocoon'), 
			'slug'               => 'revslider', 
			'source'             => get_template_directory() . '/inc/plugins/revslider.zip', 
			'required'           => true, 
			'version'            => '5.4.8',
			'force_activation'   => false,
		),
        array(
			'name'               => esc_html__( 'Envato Market', 'cocoon'), 
			'slug'               => 'envato-market', 
			'source'             => get_template_directory() . '/inc/plugins/envato-market.zip', 
			'required'           => true, 
			'force_activation'   => false,
		),
        array(
			'name'      			=> esc_html__( 'Kirki Framework', 'cocoon'),
			'slug'      			=> 'kirki',
			'required'  			=> true,
		),
        array(
			'name'      			=> esc_html__( 'WooCommerce', 'cocoon'),
			'slug'      			=> 'woocommerce',
			'required'  			=> true,
		),
        array(
			'name'               => esc_html__( 'YITH WooCommerce Wishlist', 'cocoon' ),
			'slug'               => 'yith-woocommerce-wishlist',
			'required'           => false,
		),
        array(
			'name'      			=> esc_html__( 'Contact Form 7', 'cocoon'),
			'slug'      			=> 'contact-form-7',
			'required'  			=> false,
		),
        array(
	        'name'                  => esc_html__( 'MailChimp for WordPress', 'cocoon'),
	        'slug'                  => 'mailchimp-for-wp',
	        'required'              => false,
	    ),
	);

    
    
	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
    
	$config = array(
		'id'           => 'cocoon',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'install-required-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

	);
    

	tgmpa( $plugins, $config );
}

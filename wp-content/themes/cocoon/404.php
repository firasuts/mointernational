<?php
/**
*
* @package Cocoon
*
* @since 1.0
*
* ========================
* 404 PAGE NOT FOUND - TEMPLATE FILE
* ========================
*
**/


get_header();


/* grab the url for the full size featured image */
$featured_img       = cocoon_get_option( 'cocoon_404_page_header_bg' );
$color_overlay      = cocoon_get_option( 'cocoon_page_header_color_overlay' );
$overlay_opacity    = cocoon_get_option( 'cocoon_page_header_overlay_opacity' );
?>



<!-- ===== Start of Page Header ===== -->
<?php if ( !empty($featured_img) ) { ?>
    <section class="page-header parallax" data-background="<?php echo esc_url( $featured_img ); ?>" data-color="<?php echo esc_attr($color_overlay); ?>" data-color-opacity="<?php echo esc_attr($overlay_opacity); ?>">
<?php } else { ?>
    <section class="page-header">
<?php } ?>
    <div class="container">
        <div class="row">

            <!-- Start of Page Title -->
            <div class="col-md-12 my-auto text-center">
                <h1 class="title"><?php echo cocoon_get_the_title(); ?></h1>
            </div>
            <!-- End of Page Title -->

        </div>
    </div>
</section>
<!-- ===== End of Page Header ===== -->




<!-- ===== Start of Main Wrapper ===== -->
<main class="ptb100">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-12 mx-auto text-center">
                <h3 class="mt-0"><?php esc_html_e('Page not found!', 'cocoon'); ?></h3>
                <p><?php esc_attr_e('We\'re sorry, but the page you were looking for doesn\'t exist.', 'cocoon'); ?></p>
                <?php get_search_form(); ?>

                <a href="<?php echo esc_url( home_url('/') ); ?>" class="btn btn-main mt50"><?php esc_html_e('Back to Home', 'cocoon'); ?></a>
            </div>
        </div>
    </div>
</main>
<!-- ===== End of Main Wrapper ===== -->




<?php get_footer();

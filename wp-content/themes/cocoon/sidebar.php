<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* SIDEBAR
* ========================
*     
**/

$sidebar_side = cocoon_get_option('cocoon_blog_sidebar_side');
?>




<div class="col-lg-3 col-md-4 col-sm-12 <?php echo esc_attr($sidebar_side); ?>">
    <?php if ( is_active_sidebar( 'sidebar-1' ) ) { ?>
        <div class="sidebar">
            <?php dynamic_sidebar('sidebar-1'); ?>
        </div>
    <?php } ?>
</div>
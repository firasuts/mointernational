<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* SIDEBAR SHOP
* ========================
*     
**/

$sidebar_side = cocoon_get_option('cocoon_shop_sidebar_side');
?>




<div class="col-lg-3 col-md-4 col-sm-12 <?php echo esc_attr($sidebar_side); ?>">
    <?php if ( is_active_sidebar( 'sidebar-shop' ) ) { ?>
        <div class="sidebar">
            <?php dynamic_sidebar('sidebar-shop'); ?>
        </div>
    <?php } ?>
</div>
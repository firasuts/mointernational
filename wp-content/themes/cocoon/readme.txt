Cocoon - Modern WooCommerce WordPress Theme
------------------------------

Version 1.1.0
    Added:
        - Sidebar Side Option for Shop & Blog
        - Disable Sidebar for Blog
        - Hide Product Categories Option
        - Product Category Page Header Image Option
        - Boxed Layout Option
        - Wrapper Color Option
        
    Fixed:
        - Select Body Color
        
    Updated:
        - POT files


Version 1.0.2
    Fixed:
        - Small issue in core plugin


Version 1.0.1
    Added:
        - Cart Quantity on Mobile
        
    Improved:
        - Responsive.css
        - Checkboxes

Version 1.0
    - Initial
<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* TEMPLATE FOR DISPLAYING THE COMMENTS
* ========================
*     
**/


if ( post_password_required() ) {
   return;
}
?>


<!-- ===== Start of Comments ===== -->
<section class="col-12 comments" id="comments">
    <?php if ( have_comments() ) { ?>
        <h4><?php comments_number( esc_html__('no comments', 'cocoon'), esc_html__('1 comment', 'cocoon'), esc_html__('% comments', 'cocoon') ); ?></h4>

        <?php cocoon_get_comment_navigation(); ?>

        <ul class="comment-list">
            <?php
                wp_list_comments( array(
                    'style'      => 'ul',
                    'short_ping' => true,
                    'callback' => 'cocoon_comment',
                ) );
            ?>
        </ul><!-- .comment-list -->

        <?php cocoon_get_comment_navigation();
                                  
    } //Check for have_comment().
    
    
    if ( ! comments_open() && get_comments_number()  ) { // Small message if comments are closed. ?>
        <h6 class="no-comments"><?php esc_html_e( 'Comments are closed.', 'cocoon' ); ?></h6>
    <?php }
    

    comment_form(); ?>
</section>
<!-- ===== End of Comments ===== -->
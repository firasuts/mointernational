<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* SEARCH FORM 
* ========================
*     
**/
?>


<form class="main-search-form" role="search" method="get" action="<?php echo esc_url(home_url( '/' )); ?>" autocomplete="off">
	<input type="search" name="s" class="form-control" placeholder="<?php esc_attr_e('To search hit enter', 'cocoon') ?>" value="<?php echo get_search_query() ?>" />
</form>
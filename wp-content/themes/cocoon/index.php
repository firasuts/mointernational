<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* MAIN TEMPLATE FILE
* ========================
*     
**/



get_header(); 



/* grab the url for the full size featured image */
$featured_img       = cocoon_get_option( 'cocoon_blog_page_header_bg' );
$color_overlay      = cocoon_get_option( 'cocoon_page_header_color_overlay' );
$overlay_opacity    = cocoon_get_option( 'cocoon_page_header_overlay_opacity' );
$blog_title         = cocoon_get_option( 'cocoon_blog_title' );
$sidebar            = cocoon_get_option( 'cocoon_blog_sidebar' );

if ( $sidebar == 1 ) {
    $blog_align = '';
} else {
    $blog_align = 'justify-content-center';
}
?>



<!-- ===== Start of Page Header ===== -->
<?php if ( !empty($featured_img) ) { ?>
    <section class="page-header parallax" data-background="<?php echo esc_url( $featured_img ); ?>" data-color="<?php echo esc_attr($color_overlay); ?>" data-color-opacity="<?php echo esc_attr($overlay_opacity); ?>">
<?php } else { ?>
    <section class="page-header">
<?php } ?>
    <div class="container">
        <div class="row">

            <!-- Start of Page Title -->
            <div class="col-md-12 my-auto text-center">
                <h1 class="title"><?php echo esc_html($blog_title); ?></h1>
                
                <?php if ( cocoon_get_option( 'cocoon_general_breadcrumbs' ) == 1 ) {
                    echo cocoon_breadcrumbs();
                } ?>
            </div>
            <!-- End of Page Title -->

        </div>
    </div>
</section>
<!-- ===== End of Page Header ===== -->




<!-- ===== Start of Main Wrapper ===== -->
<main class="ptb100">
    <div class="container">
        <div class="row <?php echo esc_attr($blog_align); ?>">
            <div class="col-lg-9 col-md-8 col-sm-12 cocoon-posts-container">

                <?php
                if( have_posts() ) {
                    while( have_posts() ): the_post();
                        get_template_part( 'templates/content/content', get_post_format() );
                    endwhile;
                    
                    cocoon_paging_nav();
                    
                } else { // If there are no posts available ?>
                    <div class="alert alert-info" role="alert">
                        <?php esc_html_e( 'There are no posts available.', 'cocoon' ); ?>
                    </div>
                <?php } ?>
            </div>

            <?php 
            if ( $sidebar == 1 ) {
                get_sidebar();
            } ?>
        </div>
    </div>
</main>
<!-- ===== End of Main Wrapper ===== -->


<?php get_footer(); ?>
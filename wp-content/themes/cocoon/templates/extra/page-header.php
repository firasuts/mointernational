<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* PAGE HEADER TEMPLATE
* ========================
*     
**/



/* grab the url for the full size featured image */
$featured_img       = get_the_post_thumbnail_url( get_the_ID(), 'full' );
$color_overlay      = cocoon_get_option( 'cocoon_page_header_color_overlay' );
$overlay_opacity    = cocoon_get_option( 'cocoon_page_header_overlay_opacity' );
?>



<!-- ===== Start of Page Header ===== -->
<?php if ( !empty($featured_img) ) { ?>
    <section class="page-header parallax" data-background="<?php echo esc_url( $featured_img ); ?>" data-color="<?php echo esc_attr($color_overlay); ?>" data-color-opacity="<?php echo esc_attr($overlay_opacity); ?>">
<?php } else { ?>
    <section class="page-header">
<?php } ?>
    <div class="container">
        <div class="row">

            <!-- Start of Page Title -->
            <div class="col-md-12 my-auto text-center">
                <h1 class="title"><?php echo cocoon_get_the_title(); ?></h1>
                
                <?php if ( cocoon_get_option( 'cocoon_general_breadcrumbs' ) == 1 ) {
                    
                    if ( cocoon_wc_is_activated() ) {
                        if ( is_product() ){
                            echo woocommerce_breadcrumb();
                        } else {
                            echo cocoon_breadcrumbs();
                        }
                    } else {
                        echo cocoon_breadcrumbs();
                    }
    
                } ?>
            </div>
            <!-- End of Page Title -->
            
        </div>
    </div>
</section>
<!-- ===== End of Page Header ===== -->
<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* POPUPS TEMPLATE
* ========================
*     
**/


$back_top   = cocoon_get_option( 'cocoon_back_top' );
$preload    = cocoon_get_option( 'cocoon_preloader' );
?>




<!-- =============== START OF RESPONSIVE - MAIN NAV =============== -->
<nav id="main-mobile-nav"></nav>
<!-- =============== END OF RESPONSIVE - MAIN NAV =============== -->


<?php 
if ( $back_top == true ) { ?>
    <!-- ===== Start of Back to Top Button ===== -->
    <div id="backtotop">
        <a href="#"></a>
    </div>
    <!-- ===== End of Back to Top Button ===== -->
<?php }


if ( $preload == 1 ) { ?>
    <!-- ===== Start of Preloader ===== -->
    <div id="preloader"><div data-loader="circle-side"></div></div>
    <!-- ===== End of Preloader ===== -->
<?php }


if ( shortcode_exists( 'woocommerce_my_account' ) && !is_user_logged_in()  ) { ?>
    <!-- ===== Start of Login Modal ===== -->
    <div id="login-modal" class="login-modal" tabindex="-1" role="dialog">
        
        <h5><?php esc_html_e( 'My Account', 'cocoon'); ?></h5>
        <div class="container">
            <?php echo do_shortcode( '[woocommerce_my_account]' ) ?>
        </div>
        <a href="#" class="close-modal"><i class="icon-close"></i></a>
    </div>
    <!-- ===== End of Login Modal ===== -->
<?php }
<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* TEMPLATE FOR DISPLAYING A MESSAGE WHEN NO POST CAN BE FOUND
* ========================
*     
**/

?>

<h3 class="mt-0"><?php esc_attr_e( 'Nothing found!', 'cocoon' ); ?></h3>
<p class="mt20"><?php esc_attr_e( 'Sorry, but nothing matched your search terms. Please search again.', 'cocoon' ); ?></p>

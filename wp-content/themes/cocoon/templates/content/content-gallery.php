<?php

/**
*
* @package Cocoon
*
* @since 1.0.0
* 
* ========================
* GALLERY POST FORMAT
* ========================
*     
**/

?>


<div class="col-12">
    <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post blog-post-gallery'); ?>>


        <?php 
        if ( function_exists( 'cocoon_plugin_functions' ) ) {
        
            $images = rwmb_meta( 'cocoon_blog_gallery', 'type=image_advanced&size=blog' );

            if ( !empty($images) ) { ?>
                <!-- Blog Post Gallery Thumbnail -->
                <div class="gallery-post-wrapper">
                    <div class="owl-carousel gallery-post mb30">

                        <?php foreach ( $images as $image ) {
                            echo '<div class="item"><img src="' . esc_url($image["url"]) . '" alt="' . esc_attr($image["alt"]) . '" width="' . esc_attr($image["width"]) . '" height="' . esc_attr($image["height"]) . '"/></div>';
                        } ?>

                    </div>
                </div>
            <?php }
        } ?>


         <!-- Blog Post Description -->
        <div class="blog-desc">

            <!-- Post Title -->
            <h3 class="blog-post-title">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </h3>

            <?php echo cocoon_posted_meta(); ?>

            <?php the_excerpt(); ?>

            <a href="<?php the_permalink(); ?>" class="btn btn-main">
                <?php esc_html_e( 'Read More', 'cocoon' ); ?>
            </a>

        </div>
    </article>
</div>
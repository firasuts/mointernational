<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* STANDARD PAGE TEMPLATE
* ========================
*    
**/

global $post;


if ( function_exists( 'cocoon_plugin_functions' ) ) {
    // Show page header on single posts
    $page_header = get_post_meta($post->ID, 'cocoon_show_page_header', true);
} else {
    $page_header = 'show';
}

if ( $page_header == 'show' || empty($page_header) ) {
    get_template_part( 'templates/extra/page-header' );
} 
?>





<!-- ===== Start of Main Wrapper ===== -->
<main class="ptb100">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <?php 
                        the_content(); 

                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cocoon' ),
                            'after'  => '</div>',
                        ) );
                    ?>

                    <?php if(current_user_can('administrator')) : ?>
                        <footer class="col-md-12 entry-footer">
                            <?php edit_post_link( esc_html__( 'Edit', 'cocoon' ), '<span class="edit-link">', '</span>', null, 'btn btn-main' ); ?>
                        </footer><!-- .entry-footer -->
                    <?php endif; ?>


                    <?php if ( comments_open() || get_comments_number() ) : // If comments are open or we have at least one comment, load up the comment template.
                        comments_template();
                    endif; ?>

                </article>
            </div>
        </div>
    </div>
</main>
<!-- ===== End of Main Wrapper ===== -->
<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* QUOTE POST FORMAT
* ========================
*     
**/

?>

    <div class="col-12">
    <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post blog-post-quote'); ?>>  

        <?php
        $quote_content = get_post_meta($post->ID, 'cocoon_blog_quote_content', TRUE);
        $quote_author  = get_post_meta($post->ID, 'cocoon_blog_quote_author', TRUE);
        $quote_source  = get_post_meta($post->ID, 'cocoon_blog_quote_source', TRUE);
        $allowed_tags = wp_kses_allowed_html( 'post' ); 

        if (!empty($quote_content) && !empty($quote_author) ) : ?>
            <!-- Blog Post Quote -->
            <figure class="post-quote mb30">
                <span class="icon"></span>
                <blockquote>

                    <h4 class="quote"><?php echo esc_html( $quote_content ); ?></h4>

                    <?php if(!empty($quote_source)) { ?>
                        <a href="<?php echo esc_url( $quote_source ); ?>">
                    <?php } ?>
                            <h6 class="author">
                                <?php echo esc_html('- '); 
                                echo wp_kses($quote_author,$allowed_tags); ?>
                            </h6>
                    <?php if(!empty($quote_source)) { ?>
                        </a> 
                    <?php } ?>

                </blockquote>
            </figure>
        <?php endif; ?>
    </article>
</div>
<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* MEDIA CONTENT THAT IS INCLUDED ON SINGLE POSTS
* ========================
*     
**/



if ( 'audio' == get_post_format() ) {
    $audio = get_post_meta($post->ID, 'cocoon_blog_audio', true); 
    if (!empty($audio)) { ?>

        <!-- Embed Audio -->
        <div class="audio-wrapper mb30">
            <?php if(wp_oembed_get($audio)) { 
                    echo wp_oembed_get($audio);
                } else {
                    $allowed_tags = wp_kses_allowed_html( 'post' );
                    echo wp_kses($audio,$allowed_tags);
                } 
            ?>
        </div>
    <?php }


} elseif ( false == get_post_format() || 'image' == get_post_format() ) {
    if( has_post_thumbnail() ) { ?>
        <!-- Blog Post Thumbnail -->
        <div class="blog-thumbnail">
            <?php the_post_thumbnail(); ?>
        </div>
    <?php }


} elseif ( 'gallery' == get_post_format() ) {
    if ( function_exists( 'cocoon_plugin_functions' ) ) { ?>
        <div class="owl-carousel gallery-post mb30">
            <?php $images = rwmb_meta( 'cocoon_blog_gallery', 'type=image_advanced&size=blog' );

            foreach ( $images as $image ) {
                echo '<div class="item"><img src="' . esc_url($image["url"]) . '" alt="' . esc_attr($image["alt"]) . '" width="' . esc_attr($image["width"]) . '" height="' . esc_attr($image["height"]) . '"/></div>';
            } ?>
        </div>
    <?php 
    }

} elseif ( 'quote' == get_post_format() ) {
    $quote_content = get_post_meta($post->ID, 'cocoon_blog_quote_content', TRUE);
    $quote_author  = get_post_meta($post->ID, 'cocoon_blog_quote_author', TRUE);
    $quote_source  = get_post_meta($post->ID, 'cocoon_blog_quote_source', TRUE);
    $allowed_tags = wp_kses_allowed_html( 'post' ); 

    if (!empty($quote_content) && !empty($quote_author) ) { ?>
        <!-- Blog Post Quote -->
        <figure class="post-quote mb30">
            <span class="icon"></span>
            <blockquote>

                <h4><?php echo esc_html( $quote_content ); ?></h4>

                <?php if(!empty($quote_source)) { ?>
                    <a href="<?php echo esc_url( $quote_source ); ?>">
                <?php } ?>
                        <h6 class="pt20"><?php echo esc_html('- '); echo wp_kses($quote_author,$allowed_tags); ?></h6>
                <?php if(!empty($quote_source)) { ?>
                    </a> 
                <?php } ?>

            </blockquote>
        </figure>
    <?php }


} elseif ( 'video' == get_post_format() ) { 
    $video_embed = get_post_meta( $post->ID, 'cocoon_blog_video_embed', true );
    if ( !empty($video_embed) ) { ?>

        <!-- Embed Video -->
        <div class="embed-responsive embed-responsive-16by9 mb30">
            <?php if(wp_oembed_get($video_embed)) { 
                echo wp_oembed_get($video_embed); 
            } else {
                $allowed_tags = wp_kses_allowed_html( 'post' );
                echo wp_kses( $video_embed, $allowed_tags );
            } ?>
        </div>

    <?php }
} else {
    return;
} ?>
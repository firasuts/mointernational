<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* AUDIO POST FORMAT
* ========================
*     
**/

?>

<div class="col-12">
    <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post blog-post-audio'); ?>>

        <!-- Embed Audio -->
        <?php
        $audio = get_post_meta($post->ID, 'cocoon_blog_audio', true);
        if ($audio !== '') : ?>
            <div class="audio-wrapper mb30">
                <?php
                    if(wp_oembed_get($audio)) { 
                        echo wp_oembed_get($audio);
                    } else {
                        $allowed_tags = wp_kses_allowed_html( 'post' );
                        echo wp_kses($audio,$allowed_tags);
                    }
                ?>
            </div>
        <?php endif; ?>


        <!-- Blog Post Description -->
        <div class="blog-desc">

            <!-- Post Title -->
            <h3 class="blog-post-title">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </h3>

            <?php echo cocoon_posted_meta(); ?>

            <?php the_excerpt(); ?>

            <a href="<?php the_permalink(); ?>" class="btn btn-main">
                <?php esc_html_e( 'Read More', 'cocoon' ); ?>
            </a>

        </div>
    </article>
</div>
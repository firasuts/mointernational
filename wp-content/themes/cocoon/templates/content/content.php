<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* STANDARD POST FORMAT
* ========================
*     
**/

?>

<div class="col-12">
    <article id="post-<?php the_ID(); ?>" <?php post_class('blog-post'); ?>>

        <!-- Blog Post Thumbnail -->
        <?php if( has_post_thumbnail() ) { ?>
            <div class="blog-thumbnail mb30">

                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_post_thumbnail(); ?>
                </a>

            </div>
        <?php } ?>


        <!-- Blog Post Description -->
        <div class="blog-desc">

            <!-- Post Title -->
            <h3 class="blog-post-title">
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </h3>

            <?php echo cocoon_posted_meta(); ?>

            <?php the_excerpt(); ?>

            <a href="<?php the_permalink(); ?>" class="btn btn-main">
                <?php esc_html_e( 'Read More', 'cocoon' ); ?>
            </a>

        </div>
    </article>
</div>
<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* SINGLE POST CONTENT
* ========================
*     
**/

?>


<article id="post-<?php the_ID(); ?>" <?php post_class('blog-post-content'); ?>>


    <?php get_template_part( 'templates/content/content', 'media' ); ?>


    <!-- Blog Post Content -->
    <section class="blog-desc">

        <!-- Post Meta Info -->
        <?php
            echo cocoon_posted_meta();

            the_content();

            wp_link_pages();

            // get sharing options
            if ( cocoon_get_option( 'cocoon_post_share' ) ) {
                if ( function_exists( 'cocoon_share_media' ) ) {
                    echo cocoon_share_media();
                }
            }
        ?>

    </section>

</article>
<?php

/**
*
* @package Cocoon
*
* @since 1.0
*
* ========================
* COMMENTS NAV TEMPLATE
* ========================
*
**/

?>

<nav class="navigation comment-navigation" role="navigation">
    <h4 class="comment-navigation-title"><?php esc_html_e( 'Comment navigation', 'cocoon' ); ?></h4>

    <div class="nav-links clearfix">

        <div class="nav-previous text-right"><?php previous_comments_link( esc_html__( 'Older Comments', 'cocoon' ) ); ?></div>
        <div class="nav-next text-left"><?php next_comments_link( esc_html__( 'Newer Comments', 'cocoon' ) ); ?></div>

    </div><!-- .nav-links -->

</nav><!-- comment-nav -->

<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* TEMPLATE FOR HEADER VERSION 1
* ========================
*     
**/




if ( cocoon_get_option( 'cocoon_sticky_header' ) == true ) { 
    $sticky_header = 'sticky'; 
} else {
    $sticky_header = '';
}


if ( cocoon_get_option( 'cocoon_fullwidth_header' ) == true ) { 
    $header_width = 'container-fluid';
} else {
    $header_width = 'container';
}


$header_dark_text   = get_post_meta( get_the_ID(), 'cocoon_header_dark_text', 'true' );
$header_btns        = cocoon_get_option('cocoon_menu_buttons');
$menu_extras        = cocoon_get_option('menu_extras');
$menu_search        = in_array('search', $menu_extras);


if ( $header_dark_text == 1 ) {
    $header_dark_text = ' header-dark-text';
}

?>




<header class="header1 <?php echo esc_attr($sticky_header); echo esc_attr($header_dark_text); ?>">
    <div class="<?php echo esc_attr($header_width); ?>">

        <!-- ====== Start of Navbar ====== -->
        <nav class="navbar navbar-expand-lg">
            
            
            <?php if ( cocoon_get_option('logo') ) { ?>
                
                <a class="navbar-brand logo-wrapper" href="<?php echo esc_url( home_url('/') ); ?>" title="<?php esc_attr(bloginfo('name')); ?>" rel="home">
                    <!-- Logo -->
                    <img src="<?php echo esc_url( cocoon_get_option('logo') ); ?>" class="logo" alt="<?php esc_attr(bloginfo('name')); ?>" />
            
                    <?php if ( cocoon_get_option('logo-white') ) { ?>
                        <!-- White Logo -->
                        <img src="<?php echo esc_url( cocoon_get_option('logo-white') ); ?>" class="logo-white" alt="<?php esc_attr(bloginfo('name')); ?>" />
                    <?php } ?>
                </a>
            
            <?php } elseif ( cocoon_get_option('logo_text') ) { ?>

                <h1 class="logo">
                    <a href="<?php echo esc_url( home_url('/') ); ?>" rel="home"><?php echo esc_html( cocoon_get_option('logo_text') ); ?></a>
                </h1>

            <?php } else { ?>
            
                <a class="navbar-brand" href="<?php echo esc_url( home_url('/') ); ?>" title="<?php esc_html(bloginfo('name')); ?>" rel="home">
                    <!-- INSERT YOUR LOGO HERE -->
                    <img src="<?php echo esc_url(get_template_directory_uri()  . '/assets/images/logo.svg'); ?>" alt="<?php esc_attr(bloginfo('name')); ?>" width="150" class="logo">
                    <!-- INSERT YOUR WHITE LOGO HERE -->
                    <img src="<?php echo esc_url(get_template_directory_uri()  . '/assets/images/logo-white.svg'); ?>" alt="<?php esc_attr(bloginfo('name')); ?>" width="150" class="logo-white">
                </a>
            <?php } ?>
            
            <?php if ( cocoon_wc_is_activated() ) { 
                global $woocommerce; ?>
                <a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="mobile-cart">
                    <i class="icon-bag"></i>
                    <span class="notification-count">
                        <?php echo esc_html(intval( $woocommerce->cart->cart_contents_count )); ?>
                    </span>
                </a>
            <?php } ?>
            
            
            
            <button id="mobile-nav-toggler" class="hamburger hamburger--collapse" type="button">
               <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
               </span>
            </button>

            <!-- ====== Start of #main-nav ====== -->
            <div class="navbar-collapse" id="main-nav">
                
                
                <?php 
                // Main Menu
                wp_nav_menu( array(
                    'theme_location'    => 'primary',
                    'container'         => false,
                    'menu_class'        => 'navbar-nav main-nav mx-auto',
                    'walker'            => new Cocoon_Mega_Menu_Walker(),
                    'fallback_cb'       => 'cocoon_menu_fallback'
                ) );
                
                if( $header_btns == true ) {
                    get_template_part( '/templates/header/header-btns');
                } ?>

            </div>
            <!-- ====== End of #main-nav ====== -->
        </nav>
        <!-- ====== End of Navbar ====== -->
        
        
        <?php
        if ( $header_btns == true && $menu_search ) { ?>
            <!-- =============== START OF GENERAL SEARCH WRAPPER =============== -->
            <div class="general-search-wrapper">
                <form class="general-search" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <input type="text" class="search-field" name="s" placeholder="<?php esc_attr_e( 'Type and hit enter...', 'cocoon'); ?>" autocomplete="off">
                    <input type="hidden" name="post_type" value="product">
                    <span id="general-search-close" class="icon-close toggle-search"></span>
                </form>
                
                <div class="search-results">
                    <div class="text-center loading">
                        <span class="fa fa-spinner fa-spin"></span>
                    </div>
                    <div class="woocommerce"></div>
                </div>
            </div>
            <!-- =============== END OF GENERAL SEARCH WRAPPER =============== -->
        <?php } ?>

    </div>
</header>
<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* HEADER BUTTONS
* ========================
*     
**/



$header_btns = cocoon_get_option('menu_extras');
?>




<!-- ====== Start of Extra Nav ====== -->
<ul class="navbar-nav extra-nav">
    
    <?php
    //Show search icon in the header when it is enabled.
    if (in_array('search', $header_btns)) { ?>
    <!-- Menu Item -->
        <li class="nav-item menu-item-search">
            <a class="nav-link toggle-search" href="#">
                <i class="icon-magnifier"></i>
            </a>
        </li>
    <?php }
    
    
    //Show Wishlist in the header when it is enabled.
    if (in_array('wishlist', $header_btns)) {
        if ( function_exists( 'YITH_WCWL' ) ) { ?>
            <!-- Menu Item -->
            <li class="nav-item menu-item-wishlist">
                <a class="nav-link wishlist" href="<?php echo esc_url( get_permalink( get_option( 'yith_wcwl_wishlist_page_id' ) ) ); ?>">
                    <i class="icon-heart"></i>
                    <span class="notification-count"><?php echo esc_html(intval( YITH_WCWL()->count_products() )); ?></span>
                </a>
            </li>
        <?php }
    }
    
    
    //Show Cart in the header when it is enabled.
    if (in_array('cart', $header_btns)) {
        if ( function_exists( 'woocommerce_mini_cart' ) ) {
            
            global $woocommerce; ?>

            <!-- Menu Item -->
            <li class="nav-item menu-item-cart">
                <a class="nav-link cart" href="<?php echo esc_url( wc_get_cart_url() ); ?>">
                    <i class="icon-bag"></i>
                    <span class="notification-count">
                        <?php echo esc_html(intval( $woocommerce->cart->cart_contents_count )); ?>
                    </span>
                </a>
            </li>
        <?php }
    }
    
    
    //Show account in the header when it is enabled.
    if (in_array('account', $header_btns)) { ?>
        
        <?php
        if( cocoon_wc_is_activated() ) {
            $account_link = get_permalink( get_option('woocommerce_myaccount_page_id') );
        } else {
            $account_link = '#';
        }
        
        // If user is logged in
        if ( is_user_logged_in() ) {
            $user_id = get_current_user_id();
            ?>
            
            <!-- Menu Item -->
            <li class="nav-item menu-item-account loggedin">
                <a href="<?php echo esc_url($account_link); ?>" class="nav-link login-btn">
                    <?php echo get_avatar( get_the_author_meta( 'ID', $user_id ), 30 ); ?>
                </a>
            </li>
            
		<?php // When user is not logged in
        } else { ?>
            <!-- Menu Item -->
            <li class="nav-item menu-item-account">
                <a href="#" class="nav-link login-btn">
                    <i class="icon-user"></i>
                </a>
            </li>
		<?php }
        
    } ?>
    
</ul>
<!-- ====== End of Extra Nav ====== -->
<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );



$sidebar = cocoon_get_option('cocoon_shop_sidebar');

if ( $sidebar == 1 ) { 
    $col = 'col-lg-9 col-md-8 col-sm-12';
} else {
    $col = 'col-md-12 col-sm-12 col-12';
}



/* grab the url for the full size featured image */
$featured_img       = cocoon_get_option( 'cocoon_shop_page_header_bg' );
$color_overlay      = cocoon_get_option( 'cocoon_page_header_color_overlay' );
$overlay_opacity    = cocoon_get_option( 'cocoon_page_header_overlay_opacity' );

if (function_exists('cocoon_wc_get_cat_header_image_url')) {
   $category_featured_img = cocoon_wc_get_cat_header_image_url(); 
}  
?>



<!-- ===== Start of Page Header ===== -->
<?php if ( is_shop() && !empty($featured_img) ) { ?>
    <section class="page-header woocommerce-page-header parallax" data-background="<?php echo esc_url( $featured_img ); ?>" data-color="<?php echo esc_attr($color_overlay); ?>" data-color-opacity="<?php echo esc_attr($overlay_opacity); ?>">
        
<?php } elseif ( !empty($category_featured_img) ) { ?>
    <section class="page-header woocommerce-page-header parallax" data-background="<?php echo esc_url( $category_featured_img ); ?>" data-color="<?php echo esc_attr($color_overlay); ?>" data-color-opacity="<?php echo esc_attr($overlay_opacity); ?>">
        
<?php } else { ?>
    <section class="page-header woocommerce-page-header">
<?php } ?>
        
    <div class="container">
        <div class="row">

            <!-- Start of Page Title -->
            <div class="col-md-12 my-auto text-center">
                <h1 class="title"><?php echo cocoon_get_the_title(); ?></h1>
                
               <?php if ( cocoon_get_option( 'cocoon_general_breadcrumbs' ) == 1 ) {
                    echo woocommerce_breadcrumb();
                } ?>
            </div>
            <!-- End of Page Title -->

        </div>
    </div>
</section>
<!-- ===== End of Page Header ===== -->




<main class="ptb100">
    <div class="container">
        <div class="row">
            <div class="<?php echo esc_attr($col); ?> cocoon-shop-container">
                <?php
                /**
                 * Hook: woocommerce_before_main_content.
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 * @hooked WC_Structured_Data::generate_website_data() - 30
                 */
                do_action( 'woocommerce_before_main_content' );

                ?>

                <?php
                if ( woocommerce_product_loop() ) { ?>
                    
                    <div class="archive-loop-header">
                        <?php
                        /**
                         * Hook: woocommerce_before_shop_loop.
                         *
                         * @hooked wc_print_notices - 10
                         * @hooked woocommerce_result_count - 20
                         * @hooked woocommerce_catalog_ordering - 30
                         */
                        do_action( 'woocommerce_before_shop_loop' ); ?>
                    </div>
                    
                <?php
                    woocommerce_product_loop_start();

                    if ( wc_get_loop_prop( 'total' ) ) {
                        while ( have_posts() ) {
                            the_post();

                            /**
                             * Hook: woocommerce_shop_loop.
                             *
                             * @hooked WC_Structured_Data::generate_product_data() - 10
                             */
                            do_action( 'woocommerce_shop_loop' );

                            wc_get_template_part( 'content', 'product' );
                        }
                    }

                    woocommerce_product_loop_end();

                    /**
                     * Hook: woocommerce_after_shop_loop.
                     *
                     * @hooked woocommerce_pagination - 10
                     */
                    do_action( 'woocommerce_after_shop_loop' );
                } else {
                    /**
                     * Hook: woocommerce_no_products_found.
                     *
                     * @hooked wc_no_products_found - 10
                     */
                    do_action( 'woocommerce_no_products_found' );
                }

                /**
                 * Hook: woocommerce_after_main_content.
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                do_action( 'woocommerce_after_main_content' ); ?>
            </div>
            
            <?php
            if ( $sidebar == 1 ) {
                /**
                 * Hook: woocommerce_sidebar.
                 *
                 * @hooked woocommerce_get_sidebar - 10
                 */
                do_action( 'woocommerce_sidebar' ); 
            } ?>
        
        </div>
    </div>
</main>  

<?php get_footer( 'shop' );
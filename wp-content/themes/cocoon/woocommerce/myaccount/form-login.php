<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>


<div class="row justify-content-center" id="customer_login">
	<div class="col-md-6 col-12">
        <div class="cocoon-account-tabs">
        
            <!-- Nav Tabs for Login & Register -->
            <ul class="nav nav-pills justify-content-center" id="account-tab" role="tablist">
                <li class="nav-item">
                    <a href="#login" class="nav-link active show" id="login-tab" data-toggle="tab" role="tab" aria-controls="login" aria-selected="true"><?php esc_html_e('Login', 'cocoon'); ?></a>
                </li>

                <?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
                    <li class="nav-item">
                        <a href="#register" class="nav-link" id="register-tab" data-toggle="tab" role="tab" aria-controls="register" aria-selected="false"><?php esc_html_e('Register', 'cocoon'); ?></a>
                    </li>
                <?php endif; ?>
            </ul>
        
        
        
            <!-- Tab Content -->
            <div class="tab-content" id="account-tab-content">

                <!-- Login Tab -->
                <div class="tab-pane active show" id="login" aria-labelledby="login-tab" role="tabpanel">

                    <form class="woocommerce-form woocommerce-form-login login" method="post">

                        <?php do_action( 'woocommerce_login_form_start' ); ?>

                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <label for="username"><?php esc_html_e( 'Username or email address', 'cocoon' ); ?>&nbsp;<span class="required">*</span></label>
                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                        </p>
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                            <label for="password"><?php esc_html_e( 'Password', 'cocoon' ); ?>&nbsp;<span class="required">*</span></label>
                            <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" />
                        </p>

                        <?php do_action( 'woocommerce_login_form' ); ?>

                        <p class="form-row">
                            <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                            <button type="submit" class="woocommerce-Button btn btn-main mt10" name="login" value="<?php esc_attr_e( 'Log in', 'cocoon' ); ?>"><?php esc_html_e( 'Log in', 'cocoon' ); ?></button>
                            
                            <div class="checkboxes">
                                <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline mt10 mb-0">
                                    <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> 
                                    <span><?php esc_html_e( 'Remember me', 'cocoon' ); ?></span>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </p>
                        <p class="woocommerce-LostPassword lost_password mb-0">
                            <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'cocoon' ); ?></a>
                        </p>

                        <?php do_action( 'woocommerce_login_form_end' ); ?>

                    </form>
                </div>

                <?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
                    <!-- Register Tab -->
                    <div class="tab-pane" id="register" aria-labelledby="register-tab" role="tabpanel">

                        <form method="post" class="woocommerce-form woocommerce-form-register register">

                            <?php do_action( 'woocommerce_register_form_start' ); ?>

                            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_username"><?php esc_html_e( 'Username', 'cocoon' ); ?>&nbsp;<span class="required">*</span></label>
                                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                                </p>

                            <?php endif; ?>

                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="reg_email"><?php esc_html_e( 'Email address', 'cocoon' ); ?>&nbsp;<span class="required">*</span></label>
                                <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                            </p>

                            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_password"><?php esc_html_e( 'Password', 'cocoon' ); ?>&nbsp;<span class="required">*</span></label>
                                    <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" />
                                </p>

                            <?php endif; ?>

                            <?php do_action( 'woocommerce_register_form' ); ?>

                            <p class="woocommerce-FormRow form-row">
                                <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                                <button type="submit" class="woocommerce-Button btn btn-main mt10" name="register" value="<?php esc_attr_e( 'Register', 'cocoon' ); ?>"><?php esc_html_e( 'Register', 'cocoon' ); ?></button>
                            </p>

                            <?php do_action( 'woocommerce_register_form_end' ); ?>

                        </form>

                    </div>
                <?php endif; ?>
            
            </div>
        </div>
    </div>
</div>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>

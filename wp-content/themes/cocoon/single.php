<?php

/**
*
* @package Cocoon
*
* @since 1.0
* 
* ========================
* TEMPLATE FOR ALL SINGLE POSTS
* ========================
*     
**/

$sidebar = cocoon_get_option( 'cocoon_blog_sidebar' );

if ( $sidebar == 1 ) {
    $blog_align = '';
} else {
    $blog_align = 'justify-content-center';
}




get_header();



// Show page header on single posts
get_template_part( 'templates/extra/page-header' ); ?>



<!-- ===== Start of Main Wrapper ===== -->
<main class="ptb100">
    <div class="container">
        <div class="row <?php echo esc_attr($blog_align); ?>">
            
            <div class="col-lg-9 col-md-8 col-sm-12">

                <?php
                    if( have_posts() ):
                        while( have_posts() ): the_post();

                            get_template_part( 'templates/content/single', '' );

                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) :
                                comments_template();
                            endif;

                        endwhile;
                    endif;
                ?>

            </div>

            <?php
            if ( $sidebar == 1 ) {
                get_sidebar();
            } ?>
        
        </div>
    </div> <!-- .container -->
</main>
<!-- ===== End of Main Wrapper ===== -->


<?php get_footer(); ?>
/**********************

Custom.js
=============

Author:  Gino Aliaj
Theme: Cocoon - Creative Multipurpose WordPress Theme

Author URI: http://themeforest.net/user/gnodesign/

***************************/


(function ($) {

    "use strict";




    $(document).ready(function () {




        /*----------------------------------------------------
          PRELOADER
        ----------------------------------------------------*/
        function preloader_page() {
            var preloader = $('#preloader');

            $(window).on( 'beforeunload', function (e) {
                preloader.fadeIn('slow');
            });

            $(window).on( 'pageshow', function () {
                preloader.delay(350).fadeOut('slow');
            });
        }

        preloader_page();




        /*----------------------------------------------------
          HEADER ON SCROLL
        ----------------------------------------------------*/
        function header_fixed() {
            var scroll = $(window).scrollTop();
            var header = $('header.header1.sticky');

            if (scroll >= 10) {
                header.addClass("header-fixed");
            } else {
                header.removeClass("header-fixed");
            }
        }

        $(window).scroll(function() {
            header_fixed();
        });




        /*----------------------------------------------------
           MAIN MENU FOR RESPONSIVE MODE
         ----------------------------------------------------*/
        if ($("nav#main-mobile-nav").length > 0) {
            function mmenuInit() {
                if ( $(window).width() <= '1024' ) {

                    var main_nav = $("nav.navbar #main-nav ul.main-nav");

                    // Clone the main menu, remove all unneeded classes and insert on top
                    main_nav.clone().addClass("mmenu-init").prependTo("#main-mobile-nav").removeAttr('id').removeClass('navbar-nav mx-auto').find('li').removeAttr('class').find('a').removeAttr('class data-toggle aria-haspopup aria-expanded').siblings('ul.dropdown-menu').removeAttr('class style');

                    // Fix mega menu html structure
                    var mega_menu_items = $('#main-mobile-nav').find('.mega-menu-inner .menu-item-mega');
                    mega_menu_items.wrap('<li class="mega-menu-item"/>').contents().unwrap();

                    var mega_menu_item = $('#main-mobile-nav').find('li.mega-menu-item');
                    if( mega_menu_item.length > 0 ) {
                        mega_menu_item.parent().parent().parent().parent().after(mega_menu_item);
                        mega_menu_item.first().prev().remove();
                        mega_menu_item.removeAttr('class');

                        var mega_menu_submenu = mega_menu_item.find('.mega-menu-submenu');
                        mega_menu_submenu.contents().unwrap().removeAttr('class');
                    }


                    // Initializing
                    var main_menu = $( 'nav#main-mobile-nav' );

                    main_menu.mmenu({
                        extensions: [ 'fx-menu-zoom', 'position-right' ],
                        counters: true
                    }, {
                        offCanvas: {
                            //pageNodetype: "main",
                            pageSelector: ".wrapper"
                        }
                    });


                    var menu_toggler = $("#mobile-nav-toggler");
                    var menu_API = main_menu.data( "mmenu" );

                    menu_toggler.on( "click", function() {
                       menu_API.open();
                    });
                    
                    menu_API.bind( "open:finish", function() {
                       setTimeout(function() {
                          menu_toggler.addClass( "is-active" );
                       }, 100);
                    });

                    menu_API.bind( "close:finish", function() {
                       setTimeout(function() {
                          menu_toggler.removeClass( "is-active" );
                       }, 100);
                    });
                    
                }
            }

            mmenuInit();

            $(window).resize(function () {
                mmenuInit();
            });
        }





         /*----------------------------------------------------
           MOBILE MENU CLASS ON RESPONSIVE - ADMIN MODE
         ----------------------------------------------------*/
        function menu_mobile_admin_class() {
            var scroll      = $(window).scrollTop();
            var mobile_menu = $('nav#main-mobile-nav');

            if ( ! mobile_menu.length && ! $('body').hasClass('admin-bar') ) {
                return;
            }

            if ( $(window).width() <= '600' ) {
                if (scroll <= 46) {
                    mobile_menu.removeClass('not-on-top');
                } else {
                    mobile_menu.addClass('not-on-top');
                }
            } else {
                mobile_menu.removeClass('not-on-top');
            }

        }

        menu_mobile_admin_class();

        $(window).resize(function () {
            menu_mobile_admin_class();
        });

        $(window).scroll(function() {
            menu_mobile_admin_class();
        });





        /*----------------------------------------------------
          PARALLAX
        ----------------------------------------------------*/
        if("ontouchstart"in window){
            document.documentElement.className=document.documentElement.className+" touch";
        }

        function parallaxBG() {
            $('.parallax').prepend('<div class="parallax-overlay"></div>');

            $(".parallax").each(function () {
                var attrImage = $(this).attr('data-background');
                var attrColor = $(this).attr('data-color');
                var attrOpacity = $(this).attr('data-color-opacity');
                if (attrImage !== undefined) {
                    $(this).css('background-image', 'url(' + attrImage + ')');
                }
                if (attrColor !== undefined) {
                    $(this).find(".parallax-overlay").css('background-color', '' + attrColor + '');
                }
                if (attrOpacity !== undefined) {
                    $(this).find(".parallax-overlay").css('opacity', '' + attrOpacity + '');
                }
            });
        }
        parallaxBG();

        if (!$("html").hasClass("touch")) {
            $(".parallax").css("background-attachment", "fixed");
        }

        function backgroundResize() {
            var windowH = $(window).height();
            $(".parallax").each(function (i) {
                var path = $(this);
                var contW = path.width();
                var contH = path.height();
                var imgW = path.attr("data-img-width");
                var imgH = path.attr("data-img-height");
                var ratio = imgW / imgH;
                var diff = 100;
                diff = diff ? diff : 0;
                var remainingH = 0;

                if (path.hasClass("parallax") && !$("html").hasClass("touch")) {
                    remainingH = windowH - contH;
                }
                imgH = contH + remainingH + diff;
                imgW = imgH * ratio;
                if (contW > imgW) {
                    imgW = contW;
                    imgH = imgW / ratio;
                }
                path.data("resized-imgW", imgW);
                path.data("resized-imgH", imgH);
                path.css("background-size", imgW + "px " + imgH + "px");
            });
        }
        $(window).resize(backgroundResize);
        $(window).focus(backgroundResize);
        backgroundResize();

        function parallaxPosition(e) {
            var heightWindow = $(window).height();
            var topWindow = $(window).scrollTop();
            var bottomWindow = topWindow + heightWindow;
            var currentWindow = (topWindow + bottomWindow) / 2;

            $(".parallax").each(function (i) {
                var path = $(this);
                var height = path.height();
                var top = path.offset().top;
                var bottom = top + height;

                if (bottomWindow > top && topWindow < bottom) {
                    var imgH = path.data("resized-imgH");
                    var min = 0;
                    var max = -imgH + heightWindow;
                    var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow;
                    top = top - overflowH;
                    bottom = bottom + overflowH;
                    var value = 0;

                    if ($('.parallax').is(".titlebar")) {
                        value = min + (max - min) * (currentWindow - top) / (bottom - top) * 2;
                    } else {
                        value = min + (max - min) * (currentWindow - top) / (bottom - top) * 1.5;
                    }
                    var orizontalPosition = path.attr("data-oriz-pos");
                    orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
                    $(this).css("background-position", orizontalPosition + " " + value + "px");
                }
            });
        }

        if (!$("html").hasClass("touch")) {
            $(window).resize(parallaxPosition);
            $(window).scroll(parallaxPosition);
            parallaxPosition();
        }





        /*----------------------------------------------------
          GENERAL SEARCH FORM
        ----------------------------------------------------*/
        function general_search() {
            var search_btn = $( 'header.header1 .extra-nav .toggle-search' );
            var general_searchform = $( 'header.header1 .general-search-wrapper' );
            var search_close = $( 'header.header1 .general-search-wrapper .toggle-search' );

            search_btn.on( 'click', function(e){
                e.preventDefault();

                general_searchform.addClass('open');
            });

            search_close.on( 'click', function(){
                general_searchform.removeClass('open');
            });
        }

        general_search();





        /*----------------------------------------------------
           BACK TO TOP
         ----------------------------------------------------*/
        function back_to_top() {
            var pxShow = 300;
            var scrollSpeed = 500;

            $(window).scroll(function () {
                if ($(window).scrollTop() >= pxShow) {
                    $("#backtotop").addClass('visible');
                } else {
                    $("#backtotop").removeClass('visible');
                }
            });

            $('#backtotop a').on('click', function () {
                $('html, body').animate({
                    scrollTop: 0
                }, scrollSpeed);
                return false;
            });
        }

        back_to_top();




        /*----------------------------------------------------
          GALLERY POST SLIDER
        ----------------------------------------------------*/
        function gallery_post_slider() {
            var gallery_post = $('body').find('.owl-carousel.gallery-post');

            gallery_post.imagesLoaded( function() {
                // Initializing Owl Slider for Gallery Post
                gallery_post.owlCarousel({
                    items: 1,
                    loop: true,
                    autoplay: false,
                    autoHeight: true,

                    //Navigation
                    nav: true,
                    dots: true,
                    navSpeed: 800,

                });
            });
        }

        gallery_post_slider();




        /*----------------------------------------------------
          LOGO SLIDER
        ----------------------------------------------------*/
        function logo_slider() {
            var logo_slider = $(".logo-carousel");

            $(".logo-carousel").each(function(){

                    var columns  = parseInt( $(this).attr('data-columns') );
                    var autoplay = parseInt( $(this).attr('data-autoplay') );

                    if( !autoplay ) {
                        autoplay = false;
                    }

                    switch ( columns ) {
                       case 1:
                          var responsive = { 0:{ items: 1 }, 767:{ items: 1 }, 992:{ items: columns }, 1200:{ items: columns }, 1600:{ items: columns } }
                          break
                       case 2:
                          var responsive = { 0:{ items: 2 }, 767:{ items: 2 }, 992:{ items: columns }, 1200:{ items: columns }, 1600:{ items: columns } }
                          break
                       case 3:
                          var responsive = { 0:{ items: 2 }, 767:{ items: 2 }, 992:{ items: columns }, 1200:{ items: columns }, 1600:{ items: columns } }
                          break
                       case 4:
                          var responsive = { 0:{ items: 2 }, 767:{ items: 3 }, 992:{ items: columns }, 1200:{ items: columns }, 1600:{ items: columns } }
                          break
                       case 5:
                          var responsive = { 0:{ items: 2 }, 767:{ items: 3 }, 992:{ items: columns }, 1200:{ items: columns }, 1600:{ items: columns } }
                          break
                       default:
                          var responsive = { 0:{ items: 2 }, 767:{ items: 3 }, 992:{ items: columns }, 1200:{ items: columns }, 1600:{ items: columns } }
                          break
                    }

                    $(this).owlCarousel({
                        margin: 50,
                        loop: true,
                        dots: false,
                        nav: false,
                        autoplay: autoplay,
                        autoplaySpeed: 1000,
                        responsive: responsive
                    });

            });
        }

        logo_slider();





        /*----------------------------------------------------
          PRODUCT SLIDER
        ----------------------------------------------------*/
        function product_slider() {
            var products = $('.cocoon-products-carousel .cocoon-products').addClass('owl-carousel');

            products.imagesLoaded( function() {
                // Initializing Owl Slider for Gallery Post
                products.owlCarousel({
                    loop: true,
                    autoplay: false,
                    autoHeight: true,

                    //Navigation
                    nav: false,
                    dots: true,
                    navSpeed: 800,
                    responsive: {
                        0:{ items: 1 },
                        480:{ items: 2 },
                        767:{ items: 3 },
                        992:{ items: 4 },
                        1200:{ items: 4 }
                    }
                });
            });
        }

        product_slider();





        /*----------------------------------------------------
          RELATED PRODUCT SLIDER
        ----------------------------------------------------*/
        function related_product_slider() {
            $('section.related.products .cocoon-products, section.up-sells.upsells.products .cocoon-products').each(function() {
                var element = $(this);

                element.addClass('owl-carousel').owlCarousel({
                    loop: false,
                    nav: false,
                    navText: false,
                    dots: true,
                    slideBy: 1,
                    margin: 30,
                    autoplayTimeout: 5000,
                    autoHeight: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        480: {
                            items: 2
                        },
                        767: {
                            items: 3
                        },
                        992: {
                            items: 4
                        }
                    }
                });
            });

        }

        related_product_slider();





        /*----------------------------------------------------
          RELATED PRODUCT SLIDER
        ----------------------------------------------------*/
        function cross_sell_product_slider() {
            $('.woocommerce-cart .woocommerce .cross-sells .cocoon-products').each(function() {
                var element = $(this);

                element.addClass('owl-carousel').owlCarousel({
                    loop: true,
                    nav: false,
                    navText: false,
                    dots: true,
                    slideBy: 1,
                    margin: 30,
                    autoplayTimeout: 5000,
                    autoHeight: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        480: {
                            items: 2
                        },
                        767: {
                            items: 3
                        },
                        992: {
                            items: 4
                        }
                    }
                });
            });

        }

        cross_sell_product_slider();





        /*----------------------------------------------------
          MAGNIFIC POP UP
        ----------------------------------------------------*/
        $('body').magnificPopup({
            type: 'image',
            delegate: 'a.mfp-gallery',

            fixedContentPos: true,
            fixedBgPos: true,

            overflowY: 'auto',

            closeBtnInside: true,
            preloader: true,

            removalDelay: 0,
            mainClass: 'mfp-fade',

            gallery:{enabled:true},

            callbacks: {
                buildControls: function() {
                     this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                }
            }
        });


        // Init for popup-with-zoom-anim class
        $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',

            fixedContentPos: false,
            fixedBgPos: true,

            overflowY: 'auto',

            closeBtnInside: true,
            preloader: false,

            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });


        // Init for images
        $('.mfp-image').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            image: {
                verticalFit: true
            }
        });


        // Init for youtube, vimeo and google maps
        $('.popup-youtube, .popup-vimeo, .popup-video, .popup-gmaps').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,

            fixedContentPos: false
        });






        /*----------------------------------------------------
          MY ACCOUNT POP UP
        ----------------------------------------------------*/

        function account_popup() {

            var account_popup   = $('#login-modal');
            var account_btn     = $('header.header1 .extra-nav .nav-item .login-btn');
            var account_close   = account_popup.find('.close-modal');

            account_btn.on( 'click', function() {
               account_popup.addClass('show');
            });

            account_close.on( 'click', function() {
               account_popup.removeClass('show');
            });

        }

        account_popup();





        /*----------------------------------------------------
          ISOTOPE
        ----------------------------------------------------*/

        /* Photo Gallery */
        var photo_gallery = $('.photo-gallery');

        photo_gallery.imagesLoaded( function() {
            photo_gallery.isotope({
                // options
                itemSelector: '.gallery-item',
                transitionDuration: '0s',
                masonry: {
                    gutter: 10
                }
            });
        });





        /*----------------------------------------------------
          PRODUCT IMAGE HOVER
        ----------------------------------------------------*/

        $('.image-switcher > img').each(function(index, el) {
            var image = $(el).attr('src');

            $(el).parent().css('background-image', 'url(' + image + ')').addClass('img-cover');

            $(el).remove();
        });





        /*----------------------------------------------------
          BOOTSTRAP TOOLTIP
        ----------------------------------------------------*/
        function bt_tooltip() {
            if ($(window).width() >= 1024) {

                var product_btns = $('.yith-wcwl-add-to-wishlist .yith-wcwl-add-button a, .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a, .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a');

                $('[data-toggle="tooltip"]').tooltip({
                    animated: 'fade',
                    container: 'body .wrapper',
                    trigger: 'hover'
                });

                product_btns.tooltip({
                    animated: 'fade',
                    container: 'body .wrapper',
                    trigger: 'hover',
                    placement: 'left',
                    title: function() {
                        return $(this).text();
                    }
                });

            }
        }

        bt_tooltip();





        /*----------------------------------------------------
          WISHLIST AJAXIFY
        ----------------------------------------------------*/
        $(document.body).on('added_to_wishlist removed_from_wishlist', function () {
            $.ajax({
                url     : cocoon_settings.ajax_url,
                dataType: 'json',
                method  : 'post',
                data    : {
                    action: 'cocoon_update_wishlist_count'
                },
                success : function (data) {
                    $('header.header1').find('.extra-nav .menu-item-wishlist .notification-count').html(data);
                }
            });
        });





        /*----------------------------------------------------
          ADD TO CART NOTIFICATION
        ----------------------------------------------------*/
        function add_cart_notification() {
            var message = false;

            $('body').on('click', '.ajax_add_to_cart', function() {

                var src = $(this).parent().find('.loop-thumbnail img').attr('src');
                var title = $(this).parent().parent().find('.product-title').html();


                if (typeof src != 'undefined' && typeof title != 'undefined') {
                    message = '<div class="added-to-cart-notification"><div class="product_message_wrapper"><div class="product_message_image"><img src="' + src + '" alt></div><div class="product_notification_text">' + title + cocoon_settings.cart_message + '</div></div></div>';
                }
            });

            $(document).on('added_to_cart', function(event, data) {
                if (message != false) {
                    $('body').prepend(message);
                }
            });

            setTimeout(function(){
                $('body').find('.added-to-cart-notification').remove();
            }, 6000);

        }

        add_cart_notification();





        /*----------------------------------------------------
          CHANGE PRODUCT QUANTITY IN CART
        ----------------------------------------------------*/
        function product_qty() {
            $('body').on('click', '.quantity .increase, .quantity .decrease', function (e) {
                e.preventDefault();

				var qty = $(this).siblings('.qty');
				var current = parseInt(qty.val(), 10);
				var min = parseInt(qty.attr('min'), 10);
				var max = parseInt(qty.attr('max'), 10);

                min = min ? min : 1;
                max = max ? max : current + 1;

                if ( $(this).hasClass('decrease') && current > min ) {
				    qty.val(current - 1);
				    qty.trigger('change');
                }

                if ( $(this).hasClass('increase') && current < max ) {
                    qty.val(current + 1);
                    qty.trigger('change');
                }

            });
        }

        product_qty();





        /*----------------------------------------------------
          PRODUCT QUICKVIEW
        ----------------------------------------------------*/
        function product_quickview() {
            $(document).on('click', '.product-quickview', function(e) {
                e.preventDefault();

                var id = $(this).data('id'),
                    prev = '',
                    next = '',
                    btn = $(this);
                btn.addClass('loading');

                $.ajax({
                    url: cocoon_settings.ajax_url,
                    data: {
                        id: id,
                        action: 'load_quickshop_content'
                    },
                    type: 'POST',
                    success: function(data) {
                        $.magnificPopup.open({
                            items: {
                                src: '<div class="quick-view-popup zoom-anim-dialog">' + data + '</div>',
                                type: 'inline'
                            },
                            tClose: '',
                            tLoading: '<i class="fa fa-spinner fa-spin"></i>',
                            removalDelay: 300,
                            callbacks: {
                                beforeOpen: function() {
                                    this.st.mainClass = 'mfp-move-horizontal' + ' quick-view-wrapper woocommerce' + ' my-mfp-zoom-in';
                                },
                            },
                        });

                        var element = $('.cocoon-quickshop-wrapper .images-slider-wrapper');
                        if (element.find('.woocommerce-product-gallery__image').length <= 1) {
                            return;
                        }



                        var owl = element.find('.image-items').addClass('owl-carousel').owlCarousel({
                            items: 1,
                            loop: true,
                            nav: true,
                            navText: false,
                            dots: true,
                            slideBy: 1,
                            margin: 0,
                            autoplay: false,
                            autoplayTimeout: 5000,
                            onInitialized: function() {
                                element.addClass('loaded').removeClass('loading');
                            }
                        });

                    },
                    complete: function() {
                        btn.removeClass('loading');
                    },
                    error: function() {},
                });
            });
        }

        product_quickview();





        /*----------------------------------------------------
          PRODUCT THUMBNAIL SLICK SLIDER
        ----------------------------------------------------*/
        function product_thumb_slider() {

            var thumbnails = $('#product-thumbnails').find('.thumbnails');
            var images = $('#product-images');

            if (!images.length) {
                return;
            }

            // Product thumnails and featured image slider
            images.not('.slick-initialized').slick({
                slidesToShow  : 1,
                slidesToScroll: 1,
                infinite      : false,
                asNavFor      : thumbnails,
                prevArrow     : '<span class="icon-arrow-left slick-prev-arrow"></span>',
                nextArrow     : '<span class="icon-arrow-right slick-next-arrow"></span>'
            });

            thumbnails.not('.slick-initialized').slick({
                slidesToShow  : 4,
                slidesToScroll: 1,
                asNavFor      : images,
                focusOnSelect : true,
                vertical      : true,
                infinite      : false,
                prevArrow     : '<span class="icon-arrow-up slick-prev-arrow"></span>',
                nextArrow     : '<span class="icon-arrow-down slick-next-arrow"></span>',
                responsive    : [
                    {
                        breakpoint: 1024,
                        settings  : {
                            slidesToShow  : 3,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });

            images.imagesLoaded(function () {
                images.addClass('loaded');
            });

            if (cocoon_settings.product_zoom === '1') {
                images.find('.photoswipe').each(function () {
                    $(this).zoom({
                        url  : $(this).attr('href'),
                        touch: false
                    });
                });
            }
        }

        product_thumb_slider();





        /*----------------------------------------------------
          PHOTOSWIPE LIGHTBOX
        ----------------------------------------------------*/
        function productGallery() {
            var images = $('#product-images');

            if (!images.length) {
                return;
            }

            var links = images.find('a.photoswipe');

            images.on('click', 'a.photoswipe', function (e) {
                e.preventDefault();

                var items = [];

                links.each(function () {
                    if ( $(this).hasClass('video') ) {
                        items.push({
                            html: $(this).data('href')
                        });

                    } else {
                        items.push({
                            src: $(this).attr('href'),
                            w  : $(this).find('img').attr('data-large_image_width'),
                            h  : $(this).find('img').attr('data-large_image_height')
                        });
                    }

                });

                var index = links.index($(this)),
                    options = {
                        index              : index,
                        bgOpacity          : 0.85,
                        showHideOpacity    : true,
                        mainClass          : 'pswp--minimal-dark',
                        barsSize           : {top: 0, bottom: 0},
                        captionEl          : false,
                        fullscreenEl       : false,
                        shareEl            : false,
                        tapToClose         : true,
                        tapToToggleControls: false
                    };

                var lightBox = new PhotoSwipe(document.getElementById('pswp'), window.PhotoSwipeUI_Default, items, options);
                lightBox.init();
            });
        }

        productGallery();





        /*----------------------------------------------------
          CART CANVAS PANEL
        ----------------------------------------------------*/

        function cart_canvas_panel() {
            var body    = $('body');
            var canvas  = $('.cocoon-off-canvas-panel');

            $('header').on('click', '.cart', function(e) {
                e.preventDefault();

                if ( body.hasClass('open-canvas-panel') ) {
                    body.removeClass('open-canvas-panel');
                } else {
                    if ($('body').hasClass('woocommerce-checkout') || $('body').hasClass('woocommerce-cart')) {
                        return false;
                    }
                    body.addClass('open-canvas-panel');
                }

            });


            // Close the Canvas
            body.on('click touchstart', '.close-canvas-panel, .off-canvas-layer', function(e) {
                e.preventDefault();

                if ( body.addClass('open-canvas-panel') ) {
                    body.removeClass('open-canvas-panel');
                }
            });

            // Open Cart when notification is clicked
            $('body').on('click', '.added-to-cart-notification', function(e) {
                $('.added-to-cart-notification').hide();
                $('header').find('.cart').trigger('click');
                e.preventDefault();
            });

        }

        cart_canvas_panel();





        /*----------------------------------------------------
          AJAX PRODUCTS SEARCH
        ----------------------------------------------------*/

        function product_instant_search() {

            if ( cocoon_settings.ajax_search === '0' || cocoon_settings.wc_active === '0' ) {
                return;
            }


            var xhr = null;
            var searchCache = {};
            var $modal = $('.general-search-wrapper');
            var $form = $modal.find('form');
            var $search = $form.find('input.search-field');
            var $results = $modal.find('.search-results');

            $modal.on( 'keyup', '.search-field', function (e) {
                var valid = false;

                if (typeof e.which == 'undefined') {
                    valid = true;
                } else if (typeof e.which == 'number' && e.which > 0) {
                    valid = !e.ctrlKey && !e.metaKey && !e.altKey;
                }

                if (!valid) {
                    return;
                }

                if (xhr) {
                    xhr.abort();
                }

                search();
            }).on( 'focusout', 'input.search-field', function () {
                if ($search.val().length < 2) {
                    $results.find('.woocommerce').slideUp(function () {
                        $modal.removeClass('searching searched actived found-products found-no-product invalid-length');
                    });
                }
            });

            out_search();



            /* Search Function */
            function search() {
                var keyword = $search.val();
                var cat = '';


                if (keyword.length < 2) {
                    $modal.removeClass('searching found-products found-no-product').addClass('invalid-length');
                    return;
                }

                $modal.removeClass('found-products found-no-product').addClass('searching');

                var keycat = keyword + cat;

                if (keycat in searchCache) {
                    var result = searchCache[keycat];

                    $modal.removeClass('searching');

                    $modal.addClass('found-products');

                    $results.find('.woocommerce').html(result.products);

                    $(document.body).trigger('cocoon_ajax_search_request_success', [$results]);

                    $results.find('.woocommerce, .buttons').slideDown(function () {
                        $modal.removeClass('invalid-length');
                    });

                    $modal.addClass('searched actived');
                } else {
                    xhr = $.ajax({
                        url     : cocoon_settings.ajax_url,
                        dataType: 'json',
                        method  : 'post',
                        data    : {
                            action: 'cocoon_search_products',
                            nonce : cocoon_settings.nonce,
                            term  : keyword,
                            cat   : cat
                        },
                        success : function (response) {
                            var $products = response.data;

                            $modal.removeClass('searching');


                            $modal.addClass('found-products');

                            $results.find('.woocommerce').html($products);

                            $results.find('.woocommerce, .buttons').slideDown(function () {
                                $modal.removeClass('invalid-length');
                            });

                            $(document.body).trigger('cocoon_ajax_search_request_success', [$results]);

                            // Cache
                            searchCache[keycat] = {
                                found   : true,
                                products: $products
                            };


                            $modal.addClass('searched actived');
                        }
                    });
                }
            }



            /* Function for clicking out of the search */
            function out_search() {
                var $modal = $('.general-search-wrapper');
                var $search = $modal.find('input.search-field');

                if ($modal.length <= 0) {
                    return;
                }

                $(window).on('scroll', function () {
                    if ( $(window).scrollTop() > 10) {
                        $modal.removeClass('show found-products searched');
                    }
                });

                $modal.on('click', '#general-search-close', function (e) {
                    if ($modal.hasClass('actived')) {
                        e.preventDefault();

                        $search.val('');
                        $modal.removeClass('searching searched actived found-products found-no-product invalid-length');
                    }
                });
            }
        }

        product_instant_search();





        /*----------------------------------------------------
          SHOP GRID & LIST VIEW MODES
        ----------------------------------------------------*/

        function shop_grid_list() {

            var shop_products   = $('.woocommerce .cocoon-products');
            var grid_btn        = $('.woocommerce .archive-loop-header nav.grid_list_nav #grid');
            var list_btn        = $('.woocommerce .archive-loop-header nav.grid_list_nav #list');

            // When Grid Button is clicked
            grid_btn.on("click", function() {
                if( $(this).hasClass("active") ){
                    return false;
                }

                $(this).addClass("active");
                list_btn.removeClass("active");

                shop_products.fadeOut(300, function() {
                    $(this).addClass("grid-products").removeClass("list-products").fadeIn(300);
                });

                return false;
            });


            // When List Button is clicked
            list_btn.on("click", function() {
                if( $(this).hasClass("active") ){
                    return false;
                }

                $(this).addClass("active");
                grid_btn.removeClass("active");

                shop_products.fadeOut(300, function() {
                    $(this).addClass("list-products").removeClass("grid-products").fadeIn(300);
                });

                return false;
            });

        }

        shop_grid_list();








    }); //end of document ready function

})(jQuery);

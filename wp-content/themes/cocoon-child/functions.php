<?php
/**
*
* @package Cocoon
* 
* ========================
* COCOON CHILD THEME FUNCTION FILE
* ========================
*     
**/



function cocoon_child_enqueue_scripts() {
	wp_enqueue_style( 'cocoon-child-style', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'cocoon_child_enqueue_scripts', 20 );